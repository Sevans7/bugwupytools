"""
Take .xlsx file with names, find email in address book
and return a copy of the file with them filled in.
Will add a column of notes/logs of results
"""
import BUGWUpytools as bp
import numpy as np
import pickle
import difflib
from tqdm import tqdm

# Pass in name, original email, and address book (dict with keys as name)
# returns email found and "results" based on comparison between original email
def getemail(addressbook, name, nickname="", email_orig=""):
    try:
        abkname, email = None, addressbook[name]
    except:
        abkname, email = fuzzymatching(addressbook, name, nickname=nickname)

    # not in address book
    if email == None:
        if email_orig == "":
            result = "ERR: No match in Address Book"
        else:
            result = ("CHECK: Discrepancy found ==> "
                    "No match in Address Book")

    # multiple matches
    elif ',' in email:
        # email provided, but not in list of matched emails
        if not(email_orig == "" or email_orig in email):
            result = f"CHECK: Discrepancy found ==> {email}"
            email = None
        # email provided, and in list of matched emails
        elif not(email_orig == "") and email_orig in email:
            result = "CONFIRMED"
            email = email_orig
        # email not provided
        else:
            result = f"ERR: Multiple matches ==> {email}"
            email = None

    # email found and no provided email
    elif email_orig == "":
        result = "NEW"

    # email found and matches provided email
    elif email_orig == email:
        result = "CONFIRMED"

    # email found and does not match provided email
    else:
        result = f"CHECK: Discrepancy found ==> {email}"
        email = None

    if not abkname == None:
        if result == "NEW":
            result = "CHECK: Name in address book is different"
        else:
            result += "/ CHECK: Name in address book is different" 

    return result, email, abkname

# attempt fuzzy matching in various ways
def fuzzymatching(addressbook, name, nickname=""):
    lastname, firstname = name[0], name[1]

    # try matching nicknames
    if not nickname == "":
        try:
            email = addressbook[(lastname, nickname)]
            if not "," in email:
                return ((lastname, nickname), email)
        except:
            pass

    # for multiple-word names, try different ordering or omitting words
    guess = []
    if " " in lastname:
        lnwords = lastname.split(" ")
        n_words = len(lnwords)
        for n in range(1, n_words):
            # move first few words of last name to first name
            fn = f'{firstname} {" ".join(lnwords[:n])}'
            ln = " ".join(lnwords[n:])
            guess.append((ln, fn))
            # disregard last few words of last name
            fn = f'{firstname}'
            ln = " ".join(lnwords[:n])
            guess.append((ln, fn))
            # disregard first few words of last name
            fn = f'{firstname}'
            ln = " ".join(lnwords[n:])
            guess.append((ln, fn))
    if " " in firstname:
        fnwords = firstname.split(" ")
        n_words = len(fnwords)
        for n in range(1, n_words):
            # move last few words of first name to last name
            fn = " ".join(fnwords[:n])
            ln = f'{" ".join(fnwords[n:])} {lastname}'
            guess.append((ln, fn))
            # disregard last few words of first name
            fn = " ".join(fnwords[:n])
            ln = f'{lastname}'
            guess.append((ln, fn))

    emails = []
    for abkname in guess:
        try:
            email = addressbook[abkname]
            emails.append((abkname, email))
        except:
            pass
    #if len(emails) == 1 and not "," in emails[0][1]:
    #    return emails[0]
    if len(emails) == 1:
        return emails[0]

    # try matching similar names
    abknames = get_close_matches_tup(name, addressbook.keys(), n=1, cutoff=0.8)
    if len(abknames) == 1:
        abkname = abknames[0]
        try:
            email = addressbook[abkname]
            return (abkname, email)
            #if not "," in email:
            #    return (abkname, email)
        except:
            pass

    return None, None

# imitate difflib.get_close_matches() for tuples
def get_close_matches_tup(word, possibilities, n=3, cutoff=0.6):
    result = []
    s = difflib.SequenceMatcher()
    s.set_seq2(f"{word[1]} {word[0]}")
    for x in possibilities:
        s.set_seq1(f"{x[1]} {x[0]}")
        if s.real_quick_ratio() >= cutoff and \
           s.quick_ratio() >= cutoff and \
           s.ratio() >= cutoff:
            result.append((s.ratio(), x))

    result = sorted(result, key=lambda tup: tup[0], reverse=True)[:n]
    return [p for (s, p) in result]


if __name__ == "__main__":
    import argparse
    argParser = argparse.ArgumentParser(
            description='Creates filled version of excel file with emails.')
    argParser.add_argument('f', action='store', type=str,
                           help='excel file with names to get emails for.')
    args = argParser.parse_args()

    # Address Book: 
    # Dictionary with (Surname, GivenName) as keys, Email(str) as values
    # Emails are separated by commas if multiple ppl have exact same names
    with open('contacts.pkl', 'rb') as f:
        addressbook = pickle.load(f)

    # Load sheet of names to fill
    sheet_dict = bp.Chart.from_excel(args.f)
    chart = sheet_dict['UntitledSheet']
    ncol_i = chart.ncol 
    chart = chart.append_empty_cols(3) # "address book names" and "notes" column

    # Which columns have names/emails?
    col_firstname = 3
    col_lastname = 4
    #col_nickname = 5
    col_email = 5
    col_abk_firstname = ncol_i
    col_abk_lastname = ncol_i+1
    col_notes = ncol_i+2

    # Find email and fill
    for i in tqdm(range(len(chart))):
        name = (chart[i, col_lastname], chart[i, col_firstname])
        #nickname = chart[i, col_nickname]
        nickname = ""
        email_orig = chart[i, col_email]

        result, email, abkname = getemail(addressbook, name,
                                    nickname=nickname, email_orig=email_orig)

        # write into chart
        chart[i, col_notes] = result
        if not email == None:
            chart[i, col_email] = email
        if not abkname == None:
            chart[i, col_abk_lastname] = abkname[0]
            chart[i, col_abk_firstname] = abkname[1]

    # Find duplicate emails
    allemails = [i for i in chart.get_col(col_email) if i != ""]
    uniqueemails = np.unique(allemails)

    dupemails = allemails.copy()
    for e in uniqueemails:
        dupemails.pop(dupemails.index(e))

    for e in dupemails:
        locs = chart.where(e)
        for (r, c) in locs:
            #print(chart[r, 1], chart[r, col_firstname], chart[r, col_lastname],
            #      chart[r, col_notes])
            chart[r, col_notes] += " / ERR: Duplicate email with diff person"

    # Save
    chart.to_excel(args.f.replace('.xlsx', '_filled.xlsx'))

    # Count stats
    n_new, n_confirmed, n_nomatch, n_wrong, n_multiple = 0, 0, 0, 0, 0
    n_wrong_nomatch, n_dup, n_diffname = 0, 0, 0
    for note in chart.get_col(col_notes):
        if note == "NEW": n_new += 1
        elif note == "CONFIRMED": n_confirmed += 1
        elif "Multiple" in note: n_multiple += 1
        elif "No match" in note and "Discrepancy" in note: n_wrong_nomatch += 1
        elif "No match" in note: n_nomatch += 1
        elif "Discrepancy" in note: n_wrong += 1
        elif "Duplicate" in note: n_dup += 1 
        elif "Name in address book" in note: n_diffname += 1
        else: print("Unknown result : ", note)

    # Print stats
    print(f"Out of {len(chart)} entries...")
    print(f" * {n_new} new emails were entered")
    print(f" * {n_confirmed} pre-existing emails where confirmed")
    print(f" * {n_nomatch+n_multiple} names/emails cannot be found")
    print(f"   -> {n_multiple} people have multiple matches")
    print(f"   -> {n_nomatch} people cannot be found in address book")
    print(f" * {n_wrong+n_wrong_nomatch+n_diffname+n_dup} names/emails need to be checked")
    print(f"   -> {n_wrong} pre-existing emails that don't match address book")
    print(f"   -> {n_wrong_nomatch} pre-existing emails but can't be found in address book")
    print(f"   -> {n_diffname} names were different in address book")
    print(f"   -> {n_dup} people have duplicate emails with another person")
