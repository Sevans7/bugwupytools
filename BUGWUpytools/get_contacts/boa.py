"""
BOA (.oab file extractor)
- original code from github.com/byteDJINN/BOA
- modified to extract just GivenName, Surname and Emails
  (and to ignore some unknown errors)
"""
from struct import unpack
from io import BytesIO
import math
import binascii
from schema import PidTagSchema
import pickle
import csv
from tqdm import tqdm

def hexify(PropID):
    return "{0:#0{1}x}".format(PropID, 10).upper()[2:]

def lookup(ulPropID):
    if hexify(ulPropID) in PidTagSchema:
        (PropertyName, PropertyType) = PidTagSchema[hexify(ulPropID)]
        return PropertyName
    else:
        return hex(ulPropID)

d = []
pkldata = {}

# When reading a binary file, always add a 'b' to the file open mode
with open('udetails.oab', 'rb') as f:
    (ulVersion, ulSerial, ulTotRecs) = unpack('<III', f.read(4 * 3))
    assert ulVersion == 32, 'This only supports OAB Version 4 Details File'
    print("Total Record Count: ", ulTotRecs)
    # OAB_META_DATA
    cbSize = unpack('<I', f.read(4))[0]
    meta = BytesIO(f.read(cbSize - 4))
    # the length of the header attributes
    # we don't know and don't really need to know how to parse these
    HDR_cAtts = unpack('<I', meta.read(4))[0]
    print("rgHdrAtt HDR_cAtts",HDR_cAtts)
    for rgProp in range(HDR_cAtts):
        ulPropID = unpack('<I', meta.read(4))[0]
        ulFlags  = unpack('<I', meta.read(4))[0]
    # these are the attributes that we actually care about
    OAB_cAtts = unpack('<I', meta.read(4))[0]
    OAB_Atts = []
    print("rgOabAtts OAB_cAtts", OAB_cAtts)
    for rgProp in range(OAB_cAtts):
        ulPropID = unpack('<I', meta.read(4))[0]
        ulFlags  = unpack('<I', meta.read(4))[0]
        OAB_Atts.append(ulPropID)
    print("Actual Count", len(OAB_Atts))
    # OAB_V4_REC (Header Properties)
    cbSize = unpack('<I', f.read(4))[0]
    f.read(cbSize - 4)

    # now for the actual stuff
    for i in tqdm(range(ulTotRecs)):
        try:
            read = f.read(4)
            if read == '':
                break
            # this is the size of the chunk, incidentally its inclusive
            cbSize = unpack('<I', read)[0]
            # so to read the rest, we subtract four
            chunk = BytesIO(f.read(cbSize - 4))
            presenceBitArray = bytearray(chunk.read(int(math.ceil(OAB_cAtts / 8.0))))
            indices = [i for i in range(OAB_cAtts)
                       if (presenceBitArray[i // 8] >> (7 - (i % 8))) & 1 == 1]
            #print("\n----------------------------------------")
            #print("Chunk Size: ", cbSize)

            def read_str():
                # strings in the OAB format are null-terminated
                buf = b""
                while True:
                    n = chunk.read(1)
                    if n == b"\0" or n == b"":
                        break
                    buf += n
                return buf.decode('utf-8') # problem here
                # return unicode(buf, errors="ignore")

            def read_int():
                byte_count = unpack('<B', chunk.read(1))[0]
                if 0x81 <= byte_count <= 0x84:
                    byte_count = unpack(
                        '<I', (chunk.read(byte_count - 0x80) + b"\0\0\0")[0:4])[0]
                else:
                    if byte_count > 127:
                        return -1
                return byte_count

            rec = {}

            for i in indices:
                PropID = hexify(OAB_Atts[i])
                if PropID not in PidTagSchema:
                    continue
                    raise ValueError(f"This property id ({PropID}) does not exist in the schema")

                (Name, Type) = PidTagSchema[PropID]

                if Type == "PtypString8" or Type == "PtypString":
                    val = read_str()
                    rec[Name] = val
                    #print(Name, val)
                elif Type == "PtypBoolean":
                    val = unpack('<?', chunk.read(1))[0]
                    rec[Name] = val
                    #print(Name, val)
                elif Type == "PtypInteger32":
                    val = read_int()
                    rec[Name] = val
                    #print(Name, val)
                elif Type == "PtypBinary":
                    bin = chunk.read(read_int())
                    rec[Name] = binascii.b2a_hex(bin)
                    #print(Name, len(bin), binascii.b2a_hex(bin))
                elif Type == "PtypMultipleString" or Type == "PtypMultipleString8":
                    byte_count = read_int()
                    #print (Name, byte_count)
                    arr = []
                    for i in range(byte_count):
                        val = read_str()
                        arr.append(val)
                        #print(i, "\t", val)
                    rec[Name] = arr
        
                elif Type == "PtypMultipleInteger32":
                    # This block has been breaking with original code
                    # at byte_count = 38. Unsure why.
                    # Don't need this info anyways, ignore
                    pass

                elif Type == "PtypMultipleBinary":
                    byte_count = read_int()
                    #print(Name, byte_count)
                    arr = []
                    for i in range(byte_count):
                        bin_len = read_int()
                        bin = chunk.read(bin_len)
                        arr.append(binascii.b2a_hex(bin))
                        #print(i, "\t", bin_len, binascii.b2a_hex(bin))
                    rec[Name] = arr
                else:
                    raise("Unknown property type (" + Type + ")")
                
                if Name == "Surname" and '@bu.edu' in str(rec['SmtpAddress']):
                    # At this point we should have Email, GivenName and Surname
                    # Some contacts, if they are offices or groups, do not have
                    # GivenNames and will raise an error; will ignore

                    temprec = {}
                    for r in ['SmtpAddress', 'GivenName', 'Surname']:
                        if r == 'SmtpAddress':
                            temprec['EmailAddress'] = str(rec[r])
                        else:
                            temprec[r] = str(rec[r])
                    d.append(temprec)

                    person = (temprec['Surname'], temprec['GivenName'])
                    if not person in pkldata:
                        pkldata[person] = temprec['EmailAddress']
                    else:
                        pkldata[person] = pkldata[person]+","+temprec['EmailAddress']
              
            remains = chunk.read()

        except:
            pass

print(f"Total # of contacts saved: {len(d)}")

outputname = "contacts"

# Save as .csv
with open(f'{outputname}.csv', 'w') as f:
    fieldnames = ['GivenName', 'Surname', 'EmailAddress']
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    for i in d:
        writer.writerow(i)
print(f'Saved as {outputname}.csv')

# Save as pickle file
# Will pickle dictionary with keys as tuple (Surname, GivenName)
# and values as email addresses (str),
# divided by commas if multiple ppl have the same exact name
with open(f'{outputname}.pkl', 'wb') as handle:
    pickle.dump(pkldata, handle, protocol=pickle.HIGHEST_PROTOCOL)
print(f'Saved as {outputname}.pkl')

