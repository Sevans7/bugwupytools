"""
File Purpose: build formulas for a dashboard.

[TODO]: construct dashboard from a set of stats.
Stats might include "countif, directly from data"
& Secondary-stats, e.g. "add results of stat 1 & stat 2".

[TODO][maybe]: translate stats rules from sheets into python.
    (Probably not worth the effort; probably lots of effort for very little payoff.)
"""
import functools
import re

from ..tools import InputMissingError, inputs_as_dict


def replacing_None_kw_with_self_attr(f):
    '''wraps f(self, *args, **kw) to use self._get_attrs(**{argname: val for arg in args}, **kw) instead of args & kw.'''
    @functools.wraps(f)
    def _f_but_replace_None_kw_with_attr_from_self(self, *args, **kw):
        __tracebackhide__ = True
        inputs = inputs_as_dict(f, self, *args, **kw)
        inputs = {key: val for key, val in tuple(inputs.items())[1:]}  # skip 'self' input.
        kw_use = self._get_attrs(**inputs)
        return f(self, **kw_use)
    return _f_but_replace_None_kw_with_attr_from_self


class DashboardFormulaBuilder():
    '''helpful methods for building formulas for dashboard.

    during initialization, enter any kwargs to set those attributes of self.
    Those will be default values for functions.
    Example:
        DashboardFormulaBuilder(col_label="Hello").get_col(...)
        # uses col_label="Hello" as default for col_label during get_col().
    '''
    def __init__(self, **kw):
        for key, val in kw.items():
            setattr(self, key, val)

    def _get_attrs(self, *, missing_ok=False, **kw):
        '''for each key in kw, if val is None, use getattr(self, key, None). if still None, raise InputMissingError.
        missing_ok: bool, default False. If True, use None instead of raising error.
        returns dict of values corresponding to keys.
        '''
        result = dict()
        for key, val in kw.items():
            if val is None:
                val = getattr(self, key, None)
                if (not missing_ok) and (val is None):
                    raise InputMissingError(f'{repr(key)} must either be provided, or set as attribute of self')
            result[key] = val
        return result

    @replacing_None_kw_with_self_attr
    def get_col(self, col_label=None, data_range=None, header_range=None, **kw__None):
        '''formula which returns column of values. Inputs are strings; output is a string.
        Note: string-values should contain quotation marks, e.g. '"col_label"'.
        returns "Index({data_range}, 0, Match({col_label}, {header_range}, 0))".
        '''
        return f"Index({data_range}, 0, Match({col_label}, {header_range}, 0))"

    @replacing_None_kw_with_self_attr
    def countif(self, col_label=None, condition=None, data_range=None, header_range=None, **kw__None):
        '''formula which returns number of values in col_label matching condition.
        Note: string-values should contain quotation marks, e.g. '"col_label"'.
        returns "CountIf(col, {condition})" where col=self.get_col(col_label, data_range, header_range).
        '''
        col = self.get_col(col_label, data_range, header_range)
        return f'CountIf({col}, {condition})'

    @replacing_None_kw_with_self_attr
    def countifs(self, *col_labels_and_conditions, data_range=None, header_range=None, **kw__None):
        '''formula which does countifs for the (col label, condition) tuples provided.
        Note: string-values should contain quotation marks, e.g. '"col_label"'.
        returns "Countifs(col0, {condition0}, col1, {condition1}, ...)"
            where colN = self.get_col(col_label_N, data_range, header_range)
        '''
        col_cond_pairs = []
        for col_label, condition in col_labels_and_conditions:
            col = self.get_col(col_label, data_range, header_range)
            cond = f'"{condition}"' if isinstance(condition, str) else condition
            col_cond_pairs.append((col, cond))
        col_conds_listed = ', '.join(f'{col}, {cond}' for col, cond in col_cond_pairs)
        return f'CountIfs({col_conds_listed})'

    @replacing_None_kw_with_self_attr
    def get_value_from_row_in_col(self, col_label=None, row_number=None, input_range=None, **kw__None):
        '''formula which returns value in column col_label, row row_number, from input_range.
        row_number is absolute, not relative. 1-indexed.
        Note: string-values should contain quotation marks, e.g. '"col_label"'.
        returns "HLOOKUP({col_label}, {input_range}, {row_number}, False)"
        '''
        return f"HLOOKUP({col_label}, {input_range}, {row_number}, False)"

    def get_n_left_cols(self, n_left, row_number='Row()', col_number='Column()', include_this_col=False, **kw__None):
        '''formula which returns range of n cols to the left of col_number, in row row_number.
        if include_this_col, also include this col (and include n+1 cols total).
        (use None for any kwarg to get value from self instead of default here.)
        returns 'Indirect(Address(Row(), Column()-n_left)&":"&Address(Row(), Column()-1))' by default.
        '''
        col_max = str(col_number) + ('' if include_this_col else '-1')
        return f'Indirect(Address({row_number}, {col_number} - {n_left})&":"&Address({row_number}, {col_max}))'

    def get_n_right_cols(self, n_right, row_number='Row()', col_number='Column()', include_this_col=False, **kw__None):
        '''formula which returns range of n cols to the right of col_number, in row row_number.
        if include_this_col, also include this col (and include n+1 cols total).
        (use None for any kwarg to get value from self instead of default here.)
        returns 'Indirect(Address(Row(), Column()+1)&":"&Address(Row(), Column()+n_right))' by default.
        '''
        col_min = str(col_number) + ('' if include_this_col else '+1')
        return f'Indirect(Address({row_number}, {col_min})&":"&Address({row_number}, {col_number} + {n_right}))'

    @replacing_None_kw_with_self_attr
    def get_upper_left_range(self, include_this_row=True, include_this_col=False,
                             row_number='Row()', col_number='Column()', **kw__None):
        '''formula which gives range of all cells above row_number, and to the left of col_number.
        if include_this_row, also include this row.
        if include_this_col, also include this col.
        (use None for any kwarg to get value from self instead of default here.)
        returns 'Indirect("A1:"&Address(Row(), Column()-1))', by default.
        '''
        row = str(row_number) + ('' if include_this_row else '-1')
        col = str(col_number) + ('' if include_this_col else '-1')
        return f'Indirect("A1:"&Address({row}, {col}))'

    @replacing_None_kw_with_self_attr
    def get_value_from_row_in_previous_col(self, col_label=None, row_number='Row()', **kw__None):
        '''formula which returns value in this cell's row, from the appropriate column to the left of this cell.
        Note: string-values should contain quotation marks, e.g. '"col_label"'.
        '''
        upper_left_range = self.get_upper_left_range(row_number=row_number)
        return self.get_value_from_row_in_col(col_label, row_number, upper_left_range)

    def mathprev_explicit(self, formula, prevcols, row_number='Row()'):
        '''returns formula but replacing [prevcol] replaced by looking at row_number in column labeled 'prevcol',
        ^for prevcol in prevcols. prevcol should be the label for a previous column.
        '''
        for prevcol in prevcols:
            replacing_with = self.get_value_from_row_in_previous_col(f'"{prevcol}"', row_number=row_number)
            formula = formula.replace(f'[{prevcol}]', replacing_with)
        return formula

    def mathprev(self, formula, row_number='Row()'):
        '''returns formula but replacing substrings between [] by looking at row_number in that column.
        For example, formula='[col1] - [col2]' --> formula for: (col1 - col2) using values in row_number.
        '''
        pattern = r"\[.*?\]"  # match substrings between brackets
        def repl_func(match):
            s = match.group(0)
            s = s[1:-1]  # exclude the brackets (first & last characters in s)
            return self.get_value_from_row_in_previous_col(f'"{s}"', row_number=row_number)
        result = re.sub(pattern, repl_func, formula)
        return result
