"""
File Purpose: WallThesis

For all the wall docs and all their wall charts.
"""
import os
import time

from ..tools import ImportFailed
try:
    import googleapiclient as gac
except ImportError:
    gac = ImportFailed('googleapiclient')

from .wall_doc import (
    open_walldoc,
)
from .wall_metadata import (
    WallMetadata,
)
from .wall_thesis_data import (
    WallThesisData,
)
from ..tools import (
    InputMissingError,
    alias_to_result_of,
    format_docstring, Logger, strtime_now,
)
from ..tools.pygsheets_tools.custom_pygsheets import _open_spreadsheet_paramdocs

from . import wall_chart_defaults as defaults


class WallThesis():
    '''For managing all the wall docs and their wall charts.

    making a backup means saving to the local machine all the wall charts and some meta-info.
    restoring from backup means resetting all the current wall charts to old values.

    client: pygsheets Client
        required for interacting with google sheets.
    meta: WallMetadata
        should not be altered after initialization;
        some methods assume the info in meta won't change and do caching accordingly.
    _debug: bool, default False
        if True, only reads the first 5 wall docs.
    '''
    def __init__(self, client, meta, *, _debug=False):
        self.client = client
        self.meta = meta
        self._debug = _debug

    @classmethod
    @format_docstring(paramdocs=_open_spreadsheet_paramdocs)
    def from_client_and_meta(cls, client, *, _debug=False, **kw__metadata_from_client):
        '''return new WallThesis object by reading from google docs.
        kwargs go to WallMetadata.from_client.

        client should be a pygsheets Client object.
        Must also provide a kwarg to indicate how to load metadata.
        E.g. url = url of metadata file.
        The options are:
        {paramdocs}
        '''
        meta = WallMetadata.from_client(client, **kw__metadata_from_client)
        return cls(client, meta, _debug=_debug)

    def keys(self): return tuple(self.docs_dict().keys())
    def items(self): return tuple(self.docs_dict().items())
    def values(self): return tuple(self.docs_dict().values())
    def __len__(self): return len(self.docs_dict())
    def __getitem__(self, key):
        '''returns self.docs_dict()[key].'''
        return self.docs_dict()[key]

    def docs_dict(self):
        '''returns dict of {docname: WallDoc} for all docnames indicated by self.meta.
        if _debug, stop after the first 5.
        '''
        # caching
        try:
            result = self._cached_docs_dict
        except AttributeError:
            pass  # handled below.
        else:
            if self._debug == result[0]:
                return result[1]
        # result
        result = dict()
        now = time.time()
        docinfo_list = self.meta.list_docs()
        L = len(docinfo_list)
        for i, docinfo in enumerate(docinfo_list):
            docname = docinfo.docname
            url = docinfo.url
            if self._debug and i>=5:
                break
            print(f'Loading docs_dict(); connecting to walldoc {i+1:2d} of {L}', end='\r')
            result[docname] = open_walldoc(self.client, url=url)
        print(f'Loaded docs_dict() in {time.time()-now:.2f} seconds!', ' '*40)
        # caching
        self._cached_docs_dict = (self._debug, result)
        # result
        return result

    def charts_dict(self):
        '''returns dict of {(docname, sheetname): WallChart} for (docname, sheetname) in self.meta.'''
        # caching
        try:
            result = self._cached_charts_dict
        except AttributeError:
            pass  # handled below.
        else:
            if self._debug == result[0]:
                return result[1]
        # result
        result = dict()
        docs_dict = self.docs_dict()
        docinfo_list = self.meta.list_docs()
        for i, docinfo in enumerate(docinfo_list):
            docname = docinfo.docname
            sheets = docinfo.sheets
            for sheetname in sheets:
                result[(docname, sheetname)] = docs_dict[docname][sheetname]
        # caching
        self._cached_charts_dict = (self._debug, result)
        # result
        return result

    charts = alias_to_result_of('charts_dict')

    def as_data(self, reread=False, **kw__from_wallthesis):
        '''return WallThesisData representing the data in self at this time.
        == WallThesisData.from_wallthesis(self).
        reread: bool, default False
            True --> read all the charts again, and update cached result in self
            False --> use cached result from self, if it exists.
        if _debug, only use the first 5 walldocs, to reduce number of read requests.
        '''
        if not reread:
            try:
                return self._cached_as_data
            except AttributeError:
                pass  # handled below
        result = WallThesisData.from_wallthesis(self, _debug=self._debug, **kw__from_wallthesis)
        self._cached_as_data = result
        return result

    def backup(self, dir=None, reread=False, **kw__to_excel):
        '''saves a backup of the data from self,
        in the directory provided, or wall_chart_defaults.DEFAULT_SAVEDIR if dir is None.
        returns (WallThesisData of data from self, abspath to saved file)

        reread: bool, default False
            whether to reread all the charts now, even if self.as_data() has a cached result.
        '''
        wtd = self.as_data(reread=reread)
        return (wtd, wtd.to_excel(dir=dir, **kw__to_excel))

    def uploader(self, charts_data=None, **kw):
        '''returns WallThesisUploader object to help with uploading WallThesisData to all the wall charts.'''
        return WallThesisUploader(self, charts_data=charts_data, **kw)


class WallThesisUploader():
    '''helps with uploading WallThesisData to all the wall charts.
    
    thesis: WallThesis or dict
        thesis[docname][sheetname] should be a WallChart object.
        (also, thesis.items() should provide (docname, doc) pairs,
            where doc.items() provides (sheetname, WallChart) pairs.)
    charts_data: None, WallThesisData, or dict
        charts_data[(docname, sheetname)] should be a WallChartData object.
        (also, charts_data.charts.items() should provide a list of ((docname, sheetname), chart) tuples.
            or, if charts_data.charts not available, chart_data.items() should provide that.)
        None --> user still needs to provide charts_data before or during the call to upload()
    log_filename: str
        where to save the log after completing upload successfully.
        Only saves the log at the end of the upload() function.
        To change this after creating the object, set self.logger.filename = new value.
    '''
    def __init__(self, thesis, charts_data=None, log_filename='log_upload'):
        self.thesis = thesis
        self.charts_data = charts_data
        self.logger = Logger(log_filename)
        self.reset()

    def reset(self):
        '''resets self as if we never tried uploading anything yet.'''
        self.ERRORS = []
        self.FAILED_TO_WRITE = []
        self.MISSING = []
        self.logger.reset()
        self.nextdoc = None

    @property
    def charts_data(self):
        '''charts_data.items() should provide (docname, doc) pairs,
        where doc.items() provides (sheetname, WallChartData) pairs.
        if entered as WallChartData, will use obj.charts_dict() instead.
        '''
        return self._charts_data
    @charts_data.setter
    def charts_data(self, value):
        '''set self._charts_data to the value provided.'''
        self._charts_data = value

    # # # UPLOAD # # #
    def upload(self, charts_data=None, *, restart=None, startdoc=None, dryrun=False, filter=(1,1),
               backup_dir=None, missing_ok=False):
        '''uploads data to the wall charts.
        Note: log will be saved to backup_dir, if upload() completes without crashing.
    
        charts_data: None or charts data
            see help(type(self).charts_data) for details.
            if None, use self.charts_data.
        restart: bool or None, default None
            whether to start from the "beginning", and upload ALL charts.
            None --> continue from previous stop point, or from startdoc if entered.
        startdoc: None or str
            if restart if True, IGNORE startdoc kwarg.. otherwise...
            if None, use self.nextdoc.
            if still None, start from the beginning.
        dryrun: bool, default False
            if True, don't actually upload anything, but do ALL other steps
            (e.g. logging. Use this as a sanity check before uploading.)
            (2023-10-23 - note - seems to work properly; i.e. you can trust nothing will be edited.)
        filter: bool, 'clear', or something which can imply a CellRangeIndex
            whether/how to update simple filter. see WallChart.update_values() docs for help.
            (Doesn't edit contents directly, but provides the option to sort by header.)
        backup_dir: str or None, default None
            directory for the backups which are made during the upload process.
            None --> use f'{wall_chart_defaults.DEFAULT_SAVEDIR}/backups_during_upload__{strtime_now()}'
        missing_ok: bool, default False
            whether it is ok for a chart to be missing from self.thesis (but exist in self.charts_data).
            if True, ignore any charts from self.charts_data which aren't in self.thesis.
                (i.e. if chart[dname][sname] exists but thesis[dname][sname] doesn't, skip (dname, sname).)

        while uploading, for each doc, sets self.nextdoc to the current doc.
        That way if interrupted by user, can continue from where we left off.

        returns abspath to backups directory.
        '''
        # # # setup # # #
        # starting point
        if restart:
            self.reset()
        if startdoc is None:
            startdoc = self.nextdoc
            if startdoc == False:
                raise ValueError('already uploaded! Use restart=True or self.reset() before uploading again.')
        # charts_data & chart keys
        if charts_data is None:
            charts_data = self.charts_data
        if charts_data is None:
            raise InputMissingError('must set charts_data before or during the call to upload().')
        try:
            charts = charts_data.charts
        except AttributeError:
            charts = charts_data
        # connection to google sheets
        thesis_docs_dict = self.thesis.docs_dict()  # << get it now since if we can't get it we can't upload.
        # backup directory
        if backup_dir is None:
            backup_dir = f'{defaults.DEFAULT_SAVEDIR}/backups_during_upload__{strtime_now()}'
            os.makedirs(backup_dir, exist_ok=True)
        # misc. setup
        fmt_docname = f'{{:{max(len(name) for name in self.docnames())}s}}'
        fmt_sheetname = f'{{:{max(len(name) for name in self.sheetnames())}s}}'
        t_start = time.time()
        if dryrun:
            self.log_and_print('--- DRY RUN --- CHARTS WILL NOT BE CHANGED ---')
        # # # begin uploading # # #
        for i, ((docname, sheetname), chartdata) in enumerate(charts.items()):
            if (startdoc is not None):
                # skip until docname == startdoc is found.
                if docname == startdoc:
                    startdoc = None
                else:
                    continue
            self.nextdoc = docname  # << in case we crash.
            pretty_docname = fmt_docname.format(docname)
            pretty_sheetname = fmt_sheetname.format(sheetname)
            self.log_and_print(f'writing chart {i:2d}: {pretty_docname} | {pretty_sheetname}', end='')
            # get chart object (direct connection to google sheets)
            try:
                wallchart = thesis_docs_dict[docname][sheetname]
            except KeyError:
                if missing_ok:
                    self.log_and_print(' --- MISSING')
                    self.MISSING.append((docname, sheetname))
                    continue  # ignore this chart, entirely.
                else:
                    errmsg = ('chart in charts_data but not in thesis: '
                              f'(docname, sheetname) = ({repr(docname)}, {repr(sheetname)})')
                    raise KeyError(errmsg) from None
            # upload new data
            try:
                ndiffs, _backup = wallchart.overwrite(chartdata, dir=backup_dir, dryrun=dryrun, filter=filter)
            except gac.http.HttpError as err:
                self.ERRORS.append(repr(err))
                self.FAILED_TO_WRITE.append((docname, sheetname))
                self.log_and_print(' --- FAILED')
            else:
                self.log_and_print(' | diffs =', ndiffs)
        self.log_and_print(f'Completed upload() in {time.time() - t_start:.2f} seconds!')
        self.logger.save(os.path.join(backup_dir, self.logger.filename))
        self.nextdoc = False  # << completed successfully
        return os.path.abspath(backup_dir)

    # # # DISPLAY # # #
    log = property(lambda self: self.logger.log, doc='''alias to self.logger.log''')
    log_and_print = property(lambda self: self.logger.log_and_print, doc='''alias to self.logger.log_and_print''')

    def docnames(self):
        '''returns all docnames from self.charts_data if provided, else self.thesis.'''
        d = self.charts_data if self.charts_data is not None else self.thesis
        charts = getattr(d, 'charts', d)  # try to get 'd.charts'. Just use d if that is not possible.
        return [docname for (docname, sheetname) in charts.keys()]

    def sheetnames(self):
        '''returns all sheetnames from self.charts_data if provided, else self.thesis.'''
        d = self.charts_data if self.charts_data is not None else self.thesis
        charts = getattr(d, 'charts', d)  # try to get 'd.charts'. Just use d if that is not possible.
        return [sheetname for (docname, sheetname) in charts.keys()]

    def caddresses(self):
        '''returns all (docname, sheetname) tuples from self.charts_data if provided, else self.thesis.'''
        d = self.charts_data if self.charts_data is not None else self.thesis
        charts = getattr(d, 'charts', d)  # try to get 'd.charts'. Just use d if that is not possible.
        return [caddress for caddress in charts.keys()]
