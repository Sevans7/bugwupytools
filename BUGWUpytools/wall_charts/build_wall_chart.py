"""
File Purpose: building wall charts

given data, build chart, including formatting!
This file should be updated manually to edit which columns go into the wall chart.

For functions here, wsheet refers to a WallChart (i.e. connected to a google sheet)
while chart refers to a Chart (note - not connected to a google sheet)


## SCRIPT USED FOR CONVERTING 70+ CHARTS INTO DISTRICT CHARTS ##
import BUGWUpytools as bp
client = pg.authorize(...)
td = bp.WallThesisData(...)
districts_doc = bp.SheetsDoc.from_client(client, url=...)  # load districts lookup table
districts_chart = districts_doc['Full-data_districts'].as_data()
dept2district = districts_chart.lookup_dict('Department', 'District')
dept2school = districts_chart.lookup_dict('Department', 'School')
DISTRICTS = list(np.unique(list(dept2district.values())))
# tell people their dept names, districts, and schools.
cadd2dept = td.meta.caddress2dept()
for person in td.people:
    person.dept = cadd2dept[person.caddress]
for person in td.people:
    person.district = dept2district[person.dept]
    person.school = dept2school[person.dept]
# assign dept & school as keys
for person in td.people:
    person['Department'] = person.dept
    person['School'] = person.school
# peoples grouped by district
peoples = {d: [] for d in DISTRICTS}
for person in td.people:
    peoples[person.district].append(person)
i = 0  # choose which district to build
sheet = ... # WallChart object
bp.rebuild_wall_sheet(sheet, peoples[DISTRICTS[i]], keys='STANDARD')


## EXAMPLE, FOR TESTING THIS CODE ##
# (please test the code before running it on all the sheets.)
import BUGWUpytools as bp
client = pg.authorize(...)
td = bp.WallThesisData(...)
PYG_DEV = "url to a fresh google sheet you will use for testing"
wdoc = bp.WallDoc.from_client(client, url=PYG_DEV)
DOCNAME = 'CAS Sciences ABC' # or the title of another wall doc you prefer to use
bp.rebuild_wall_doc(wdoc, tcur, docname=DOCNAME, verbose=True)
# now, the PYG_DEV will be full of that rebuilt wall doc, will the current settings;
# if it's not what you want you can adjust the code and iterate until it looks good.


## EXAMPLE, FOR APPLYING TO ALL WALL CHARTS ##
import time
META_KEY = 'key goes here'   # key from url of WallMetadata doc.
thesis = bp.WallThesis.from_client_and_meta(client, key=META_KEY)
tstart = time.time()
ERRORS = []
for wdoc in thesis.values():
    print(f'starting wdoc={repr(wdoc.title)}; time elapsed={time.time()-tstart:.2g} seconds.')
    try:
        bp.rebuild_wall_doc(wdoc, thesis_data, verbose=True)
    except Exception as err:
        ERRORS.append((wdoc, err))
        print(f'ERROR: {err}')
print(f'finished all wdocs in {time.time()-tstart:.2g} seconds!')

# BUFFER ALL SHEETS, and update filter to include everything:
for wsheet in thesis.charts.values():
    with wsheet.batching():
        wsheet.set_buffer()
        wsheet.update_filter((1,1))
"""

from ..tools import Chart


''' --------------------- Defaults / Setup --------------------- '''

AA = {  # aliases, so that the code in this file is more readable.
    'job': 'Job position, Spring 2024 (S24)',
    'will_strike_research': "Willingness to strike research (as planned with coworkers in your dept / lab)? (S24)",
    'will_strike_teach': 'Willingness to refuse to teach during strike? (S24)',
    'will_strike_grade': 'Willingness to withhold grades during strike? (S24)',
    '#meetings': 'Attendance at strike prep / escalation meetings (S24)',
    } 

BB = {  # renamed aliases
    'will_strike_research': "Willing to join researcher strike? (S24)",
    'will_strike_teach': "Willing to strike teaching? (S24)",
    'will_strike_grade': "Willing to strike grading? (S24)",
}

FF = {  # aliases for Fall 2024
    'job': 'Job position, Fall 2024 (F24)',
    'will_strike_research': "Willing to join researcher strike? (F24)",
    'will_strike_teach': "Willing to strike teaching / grading? (F24)",
    '#meetings': 'Attended department meetings? (F24)',
}


# # TEXT OPTIONS # #
# number picker
TEXT_1 = dict(color="#9900ff", font_color="white")   # color= purple
TEXT_1p1 = dict(color="#8e7cc3", font_color="white") # color= light purple
TEXT_2 = dict(color="#ffff02")   # color= yellow
TEXT_3 = dict(color="#ff9901")   # color= orange
TEXT_4 = dict(color="#e06666")   # color= pinkish red
TEXT_5 = dict(color="#980000", font_color="white")   # color= red
TEXT_0 = dict(color="#b7b7b7")   # color= gray

# good / medium / bad
TEXT_GOOD = dict(color="#b7e2cd")  # color= light green (default "green" conditional formatting)
TEXT_MID  = dict(color="#fce8b2")  # color= light yellow (default "yellow" conditional formatting)
TEXT_BAD  = dict(color="#f4c7c3")  # color= light red (default "red" conditional formatting

# pink / blue
TEXT_PINK = dict(color="#ffddf7")  # color= pink
TEXT_BLUE = dict(color="#c9daf8")  # color= light blue

# light rainbow with 9 colors
TEXT_R0 = dict(color="#dd7e6b")  # color= light red
TEXT_R1 = dict(color="#ea9999")
TEXT_R2 = dict(color="#f9cb9c")  # color= light orange
TEXT_R3 = dict(color="#ffe599")  # color= light yellow
TEXT_R4 = dict(color="#b6d7a8")  # color= light green
TEXT_R5 = dict(color="#a2c4c9")
TEXT_R6 = dict(color="#a4c2f4")  # color= light blue
TEXT_R7 = dict(color="#b4a7d6")  # color= light purple
TEXT_R8 = dict(color="#d5a6bd")


# # STANDARD COLS # #
# FROZEN COLS - freeze cols to the last column here.
COLS_FROZEN = ['Department', 'Assessment', 'First name', 'Last name', 'Nickname'] 
# DATA COLS - standard data columns
COLS_DATA = [
            # data updatedness
            'When was this row last checked', 
            # cohort
            'MA/PhD', 'Year Entered',
            # job position
            FF['job'], 'Notes (job position, F24)',
            FF['#meetings'],
            FF['will_strike_research'],
            FF['will_strike_teach'],
            'Notes (strike, F24)',
            # leadership
            'Assigned Rep(s)', 'Union Rep?', 'Steward?', 'Data access level', 'Leadership role?',
            # enrollment status
            'Enrollment status', 'Expected exit date',
            # contact info
            'Personal email', 'BU email', 'Cell', 'Contact preference', 'Notes (contacting)',
            # strike stuff (S24)
            AA['job'], 'Notes (job position)',
            AA['#meetings'],
            AA['will_strike_research'],
            AA['will_strike_teach'],
            AA['will_strike_grade'],
            'Notes (strike readiness)',
             ]
# MISC_NAMED COLS - put a solid border between the final DATA col, and the first MISC_NAMED col.
COLS_MISC_NAMED = ['Notes (misc)', 'Voted (F22)', 'Voted in SAV?',
                    'Leadership role?', 'VAN',
                   ]
COLS_STANDARD = COLS_FROZEN + COLS_DATA + COLS_MISC_NAMED  # list addition

# # DELETE COLS # #
COLS_DELETE = []

# REMOVED DUE TO CONTAINING PHYSICAL PII:
# 'Lab / Advisor', 'Office location'

# REMOVED DUE TO BEING IRRELEVANT NOW
# AA['#teach'], AA['#grade']

# # MISC COLS (helper... MISC if not in COLS_NOT_MISC.) # #
COLS_NOT_MISC = COLS_STANDARD + COLS_DELETE  # list addition

# # STANDARDIZE COLS # #
#   dict of {standard col name: list of all old colnames which map to this col}
#   if multiple old colnames appear on the same sheet, will use the first one in list here.
#   e.g. 'Lab / Advisor': ['Lab', 'Adviser'] means that 'Lab' and 'Adviser' both map
#      into this new col. If both appear, keep Lab and ignore Adviser.
#      if 'Lab / Advisor' already appears, ignore both 'Lab' and 'Adviser'.
STANDARDIZE_COLS = {}

# # RENAME COLS (after building chart) # #
#   dict of {old name: new name  for  cols being renamed}
#   if either name appears in COLS_STANDARD, it should be the old name;
#   will rename to the new name.
# RENAME_COLS = {}
RENAME_COLS = {
    'Leadership role?': 'Other leadership role(s)?',
    'Assessment': 'Vibe',
    'Notes (job position)': 'Notes (job position, S24)',
    AA['will_strike_research']: BB['will_strike_research'],
    AA['will_strike_teach']: BB['will_strike_teach'],
    AA['will_strike_grade']: BB['will_strike_grade'],
    'Notes (strike readiness)': 'Notes (strike, S24)',
    'Voted in SAV?': 'Voted (SAV)',
}

# # RENAME DATA (after building chart) # #
#   dict of {col to search: {old value: new value}}
#   if col to search appears in RENAME_COLS, use the new name here.
RENAME_DATA = {}
RENAME_DATA = {
    'Voted (F22)': {
        'Voted Yes': True,
        'Voted No': True,
        'Voted Unknown': True,
        'Unable to Vote': False,
    }
}


# # CELL BORDER # #
# format for cells with a border on the right
# [TODO] method to update only the specified formatting in the cell,
#    without touching the unspecified properties. Then we wouldn't need {wrapStrategry: 'CLIP'} here.
BORDER_RIGHT = {'borders': {'right': {'style': 'solid'}}, 'wrapStrategy': 'CLIP'}
BORDER_RIGHT__HEADER = {'borders': {'right': {'style': 'solid'}}, 'wrapStrategy': 'WRAP', 'textFormat': {'bold': True}}

# put a border to the right of each of these cols:
BORDER_RIGHT_COLS = ['When was this row last checked',
                     'Notes (job position, F24)',
                     'Notes (strike, F24)',
                     'Other leadership role(s)?',
                     'Expected exit date',
                     'Notes (contacting)',
                     'Notes (strike, S24)',
                     #'Notes (misc)',
                     ]

# # COLUMN WIDTHS # #
# To get this, set up one test chart and adjust the widths manually for the standard cols.
# use empty dict to use standard width for all cols. i.e. COL_WIDTHS = dict().
# Then, use WallChart.get_header_widths().
#   If any of these are from RENAME_COLS, use the new col name here, not the old one.
COL_WIDTHS = {
    'Department': 74,
    'Vibe': 34,
    'First name': 77,
    'Last name': 74,
    'Nickname': 62,
    'When was this row last checked': 81,
    'MA/PhD': 57,
    'Year Entered': 77,
    'Job position, Fall 2024 (F24)': 100,
    'Notes (job position, F24)': 100,
    'Attended department meetings? (F24)': 100,
    'Willing to join researcher strike? (F24)': 107,
    'Willing to strike teaching / grading? (F24)': 119,
    'Notes (strike, F24)': 100,
    'Assigned Rep(s)': 88,
    'Union Rep?': 66,
    'Steward?': 86,
    'Other leadership role(s)?': 96,
    'Data access level': 71,
    'Enrollment status': 97,
    'Expected exit date': 93,
    'Personal email': 85,
    'BU email': 91,
    'Cell': 98,
    'Contact preference': 97,
    'Notes (contacting)': 107,
    'Job position, Spring 2024 (S24)': 107,
    'Notes (job position, S24)': 100,
    'Attendance at strike prep / escalation meetings (S24)': 123,
    'Willing to join researcher strike? (S24)': 106,
    'Willing to strike teaching? (S24)': 100,
    'Willing to strike grading? (S24)': 100,
    'Notes (strike, S24)': 100,
    'Notes (misc)': 152,
    'Voted (F22)': 65,
    'Voted (SAV)': 65,
    'VAN': 77,
}


''' --------------------- Building Chart - Standard --------------------- '''

def build_chart(people, *, keys=None):
    '''returns Chart with COLS_STANDARD then misc cols based on people.
    people: list of ThesisPerson objects
        gets data from people.
        misc cols will be keys from people which are not in COLS_STANDARD or COLS_DELETE.
        we assume people[0] has all the same keys as the other people.
    keys: None, 'STANDARD', or list of str
        if provided, use only these keys, instead of people[0].keys().
        if 'STANDARD', use only COLS_STANDARD keys.
    '''
    chart = Chart.empty((len(people)+1, 0))
    if keys is None:
        KEYS = people[0].keys()  # assumes all people have same keys.
    elif keys == 'STANDARD':
        KEYS = COLS_STANDARD
    else:
        KEYS = keys
    # standard data
    col_data = dict()
    standardized = []
    for col in COLS_STANDARD:
        key = col
        if (col not in KEYS) and (col in STANDARDIZE_COLS):
            for old_col in STANDARDIZE_COLS[col]:
                if old_col in KEYS:
                    key = old_col
                    standardized.append(old_col)
                    break
        col_data[col] = [person.get(key, '') for person in people]
    chart = chart.append_col_dict(col_data)
    # misc data. (not in cols, and not in delete_cols.) assumes all people have same keys
    misc_data = {key: [person[key] for person in people] for key in KEYS
                    if key not in COLS_NOT_MISC and key not in standardized}
    if len(misc_data) > 0:
        chart = chart.append_col_dict(misc_data)
    # apply renaming
    chart = _apply_renaming(chart)
    return chart

def _apply_renaming(chart):
    '''applies RENAME_COLS and RENAME_DATA to chart; chart will be edited directly.'''
    # rename cols
    for old_label, new_label in RENAME_COLS.items():
        chart[0, chart.icol(old_label)] = new_label
    # rename data
    for col_label, renaming_dict in RENAME_DATA.items():
        col = chart.col(col_label, keep_header=True)
        icol = chart.icol(col_label)
        for old_name, new_name in renaming_dict.items():
            where_old_name = (col == old_name)
            chart[where_old_name, icol] = new_name
    return chart

def write_chart_to_sheet(chart, wsheet):
    '''overwrites wsheet data to be chart.
    Equivalent: wsheet.write_values_chart(chart, filter=(1,1))
        (also does bookkeeping internally: wsheet._set_cached_data(chart))
    '''
    with wsheet.batching(passthrough=True):
        wsheet._set_cached_data(chart)
        return wsheet.write_values_chart(chart, filter=(1,1))

def format_sheet_standard(wsheet):
    '''applies standard formatting to wsheet.
    - does 'WRAP' instead of 'CLIP' for header row.
    - sets col widths, using COL_WIDTHS.
    - Freezes rows / cols.
    - Put border after each col in BORDER_RIGHT_COLS
    '''
    with wsheet.batching(passthrough=True):
        wsheet.row_wrap_and_bold(row=1)
        wsheet.set_header_widths(COL_WIDTHS)
        wsheet.frozen_rows = 1
        wsheet.frozen_cols = len(COLS_FROZEN)
        for border_col in BORDER_RIGHT_COLS:
            _apply_border_right(wsheet, border_col)

def _apply_border_right(wsheet, col):
    '''puts border on the right side of col.'''
    wsheet.col_apply_format(col, BORDER_RIGHT__HEADER, rows=(1,1))  # header
    wsheet.col_apply_format(col, BORDER_RIGHT, rows=(2, None))  # other cells

''' --------------------- Building Chart - Special Cols --------------------- '''
# here we put any special rules for cols, including:
#  - data validation (e.g. checkboxes, or dropdown menu with choices for cell)
#  - conditional formatting
#  - basic formatting (e.g., format date as date?)  (not applied in current version).

# the functions here do not need to be in any particular order;
# the order is determined by the order in COLS_STANDARD.

SPECIAL_COL_FUNCS = []
def special_col(f):
    '''add f to list of special col funcs, then return f'''
    SPECIAL_COL_FUNCS.append(f)
    return f

def format_sheet_special(wsheet, verbose=False):
    '''applies all special col formatting funcs to wsheet.'''
    with wsheet.batching(passthrough=True):
        for func in SPECIAL_COL_FUNCS:
            if verbose: print(f'applying func: {func}' + ' '*60, end='\r')
            func(wsheet)

# # # ASSESSMENT # # #
@special_col
def format_vibe(wsheet):
    '''Vibe col. conditional formatting (here through end of frozen cols) & data validation.'''
    formatting = ((TEXT_1, "1 - Leader"),
                  (TEXT_2, "2 - Supporter"),
                  (TEXT_3, "3 - Unsure"),
                  (TEXT_4, "4 - Hostile"),
                  (TEXT_5, "5 - Anti-union Leader"),
                  (TEXT_0, "0 - Unassessed"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Vibe', COLS_FROZEN[-1],
                                              if_text_in_col=(text, 'Vibe'), **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy('Vibe', choices=validation)


# # # FALL 2024 INFO # # #
@special_col
def format_F24_job_position(wsheet):
    '''Job position (Fall 2024) col. data validation.'''
    formatting = ((TEXT_0, "On Leave"),
                  (TEXT_0, "NLE"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(FF['job'], if_text=text, **text_style)
    validation = ['RA or RF', 'TA or TF', 'Instructor of Record', 'Grader', 'Graduate Assistant', 'Writing Fellow',
                'Dean\'s Fellow', 'Dissertation Fellow', 'other fellowship', 'On Leave', 'NLE', 'other']
    wsheet.col_data_validation_easy(FF['job'], choices=validation)

@special_col
def format_F24_strike_research(wsheet):
    '''FF['will_strike_research'] col. conditional formatting & data validation'''
    formatting = ((TEXT_1, "1 - Enthusiastic"),
                  (TEXT_1p1, "? - Interested but not sure how to participate"),
                  (TEXT_2, "2 - Hesitant but willing"),
                  (TEXT_3, "3 - Unsure"),
                  (TEXT_4, "4 - Not willing"),
                  (TEXT_0, "0 - N/A"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(FF['will_strike_research'], if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy(FF['will_strike_research'], choices=validation)

@special_col
def format_F24_strike_teach(wsheet):
    '''FF['will_strike_teach'] col. conditional formatting & data validation'''
    formatting = ((TEXT_1, "1 - Enthusiastic"),
                  (TEXT_2, "2 - Hesitant but willing"),
                  (TEXT_3, "3 - Unsure"),
                  (TEXT_4, "4 - Not willing"),
                  (TEXT_0, "0 - N/A"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(FF['will_strike_teach'], if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy(FF['will_strike_teach'], choices=validation)

@special_col
def format_F24_strike_prep(wsheet):
    '''FF['#meetings'] col. conditional formatting.'''
    formatting = ((TEXT_0, "0 meetings"),
                  (TEXT_3, "1 meeting"),
                  (TEXT_2, "2 meetings"),
                  (TEXT_1, "3+ meetings"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(FF['#meetings'], if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy(FF['#meetings'], choices=validation)


# # # LEADERSHIP ROLE # # #
@special_col
def format_union_rep(wsheet):
    '''Union Rep? col. conditional formatting & data validation.'''
    formatting = ((TEXT_1, "current (2023-24)"),
                  (TEXT_2, "prior"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Union Rep?', if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy('Union Rep?', choices=validation)

@special_col
def format_steward(wsheet):
    '''Steward? col. conditional formatting & data validation.'''
    formatting = ((TEXT_1, "district steward"),
                  (TEXT_2, "department steward"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Steward?', if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy('Steward?', choices=validation)

@special_col
def format_data_access(wsheet):
    '''Data access permissions col. conditional formatting & data validation.'''
    formatting = ((TEXT_R0, "3"),
                  (TEXT_R2, "2"),
                  (TEXT_R3, "1"),
                  (TEXT_0, "0"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Data access level', if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy('Data access level', choices=validation)


# # # COHORT # # #
@special_col
def format_ma_phd(wsheet):
    '''MA/PhD col. data validation.'''
    formatting = ((TEXT_PINK, "MA"),
                  (TEXT_BLUE, "PhD"),
                  (TEXT_MID, "other"),  # e.g. Doctor of Musical Arts
                  (TEXT_0, "Unknown"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('MA/PhD', if_text=text, **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy('MA/PhD', choices=validation)

@special_col
def format_year_entered(wsheet):
    '''Year Entered col. data validation'''
    formatting = ((TEXT_R0, 'pre-2017'),
                  (TEXT_R1, '2017'),
                  (TEXT_R2, '2018'),
                  (TEXT_R3, '2019'),
                  (TEXT_R4, '2020'),
                  (TEXT_R5, '2021'),
                  (TEXT_R6, '2022'),
                  (TEXT_R7, '2023'),
                  (TEXT_R8, '2024'),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Year Entered', if_text=text, **text_style)
        validation = [text for _style, text in formatting[::-1]]  # ::-1 --> put 2024 as top choice in list.
        wsheet.col_data_validation_easy('Year Entered', choices=validation)


# # # LOGISTICAL INFO (contact info, enrollment status) # # #
@special_col
def format_enrollment_status(wsheet):
    '''Enrollment status col. conditional formatting (here & Exit date) & data validation.
    [TODO] smarter coloring for Exit date column?
    '''
    formatting = ((TEXT_GOOD, ">1 year remaining"),
                  (TEXT_MID, "in final year"),
                  (TEXT_MID, "<2 months remaining"),
                  (TEXT_BAD, "EXITED (will be removed next time data team runs cleanup script)"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Enrollment status', 'Expected exit date',
                                              if_text_in_col=(text, 'Enrollment status'), **text_style)
        validation = [text for _style, text in formatting]
        wsheet.col_data_validation_easy('Enrollment status', choices=validation)

@special_col
def format_contact_preference(wsheet):
    '''Contact preference col. conditional formatting (here, and cell & email cols) & data validation.
    [TODO] accomplish the same formatting using fewer rules?
    '''
    with wsheet.batching(passthrough=True):
        ANY = "Any"
        EMAIL = "Email (either)"
        PMAIL = "Email (personal)"
        BMAIL = "Email (BU email)"
        MLTXT = "Email or text"
        TEXT_ = "Text"
        PHONE = "Text or call"
        CALL_ = "Call"
        SLACK = "Slack"
        DNC = "Do not contact"
        validation = [ANY, SLACK, EMAIL, PMAIL, BMAIL, MLTXT, TEXT_, PHONE, CALL_, DNC]
        wsheet.col_data_validation_easy('Contact preference', choices=validation)

        # formatting - all 4 cols: Personal email | BU email | Cell | Contact preference
        formatting = ((TEXT_GOOD, ANY), (TEXT_0, SLACK), (TEXT_4, DNC))  # TEXT_4 even worse-looking than TEXT_BAD.
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Personal email', 'Contact preference',
                                              if_text_in_col=(text, 'Contact preference'), **text_style)
        # formatting - Contact preference
        formatting = (*[(TEXT_GOOD, opt) for opt in (EMAIL, MLTXT, PHONE)],
                      *[(TEXT_MID, opt) for opt in (PMAIL, BMAIL, TEXT_, CALL_)],)
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Contact preference', if_text=text, **text_style)

        # formatting - both email columns  (reduces number of rules, by joining common ones together)
        formatting = (*[(TEXT_GOOD, opt) for opt in (EMAIL,)],
                        *[(TEXT_MID, opt) for opt in (MLTXT,)],
                      *[(TEXT_BAD, opt) for opt in (TEXT_, PHONE, CALL_)],)
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Personal email', 'BU email',
                                              if_text_in_col=(text, 'Contact preference'), **text_style)
        # formatting - Personal email  (only for cases not covered by formatting_email)
        formatting = ((TEXT_GOOD, PMAIL), (TEXT_BAD, BMAIL))
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Personal email',
                                              if_text_in_col=(text, 'Contact preference'), **text_style)
        # formatting - BU email  (only for cases not covered by formatting_email)
        formatting = ((TEXT_GOOD, BMAIL), (TEXT_BAD, PMAIL))
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('BU email',
                                              if_text_in_col=(text, 'Contact preference'), **text_style)
        # formatting - Cell
        formatting = (*[(TEXT_GOOD, opt) for opt in (PHONE,)],
                      *[(TEXT_MID, opt) for opt in (MLTXT, TEXT_, CALL_)],
                      *[(TEXT_BAD, opt) for opt in (EMAIL, PMAIL, BMAIL)],)
        for text_style, text in formatting:
            wsheet.col_conditional_formatting('Cell',
                                              if_text_in_col=(text, 'Contact preference'), **text_style)


# # # SPRING 2024 INFO # # #
@special_col
def format_S24_job_position(wsheet):
    '''Job position (Spring 2024) col. data validation.'''
    formatting = ((TEXT_0, "On Leave"),
                  (TEXT_0, "NLE"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(AA['job'], if_text=text, **text_style)
    # OLD INFO -- DON'T INCLUDE VALIDATION DROPDOWN ANYMORE
    # validation = ['Instructor of Record', 'TA or TF', 'grader', 'RA or RF', 'Graduate Assistant', 'Writing Fellow',
    #             'Dean\'s Fellow', 'Dissertation Fellow', 'other fellowship', 'other', 'On Leave', 'NLE']
    # wsheet.col_data_validation_easy(AA['job'], choices=validation)

@special_col
def format_S24_strike_research(wsheet):
    '''BB['will_strike_research'] col. conditional formatting & data validation'''
    formatting = ((TEXT_1, "1 - Enthusiastic"),
                  (TEXT_1p1, "? - Interested but no established plan yet"),
                  (TEXT_2, "2 - Hesitant but willing"),
                  (TEXT_3, "3 - Unsure"),
                  (TEXT_4, "4 - Not willing"),
                  (TEXT_0, "0 - N/A"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(BB['will_strike_research'], if_text=text, **text_style)
        # OLD INFO -- DON'T INCLUDE VALIDATION DROPDOWN ANYMORE
        # validation = [text for _style, text in formatting]
        # wsheet.col_data_validation_easy(BB['will_strike_research'], choices=validation)

@special_col
def format_S24_strike_teach(wsheet):
    '''BB['will_strike_teach'] col. conditional formatting & data validation'''
    formatting = ((TEXT_1, "1 - Enthusiastic"),
                  (TEXT_2, "2 - Hesitant but willing"),
                  (TEXT_3, "3 - Unsure"),
                  (TEXT_4, "4 - Not willing"),
                  (TEXT_0, "0 - N/A"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(BB['will_strike_teach'], if_text=text, **text_style)
        # OLD INFO -- DON'T INCLUDE VALIDATION DROPDOWN ANYMORE
        # validation = [text for _style, text in formatting]
        # wsheet.col_data_validation_easy(BB['will_strike_teach'], choices=validation)

@special_col
def format_S24_strike_grade(wsheet):
    '''BB['will_strike_grade'] col. conditional formatting & data validation'''
    formatting = ((TEXT_1, "1 - Enthusiastic"),
                  (TEXT_2, "2 - Hesitant but willing"),
                  (TEXT_3, "3 - Unsure"),
                  (TEXT_4, "4 - Not willing"),
                  (TEXT_0, "0 - N/A"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(BB['will_strike_grade'], if_text=text, **text_style)
        # OLD INFO -- DON'T INCLUDE VALIDATION DROPDOWN ANYMORE
        # validation = [text for _style, text in formatting]
        # wsheet.col_data_validation_easy(BB['will_strike_grade'], choices=validation)

@special_col
def format_S24_strike_prep(wsheet):
    '''AA['#meetings'] col. conditional formatting.'''
    formatting = ((TEXT_0, "0 meetings"),
                  (TEXT_2, "1 meeting"),
                  (TEXT_1, "2+ meetings"),
                 )
    with wsheet.batching(passthrough=True):
        for text_style, text in formatting:
            wsheet.col_conditional_formatting(AA['#meetings'], if_text=text, **text_style)
        # OLD INFO -- DON'T INCLUDE VALIDATION DROPDOWN ANYMORE
        # validation = [text for _style, text in formatting]
        # wsheet.col_data_validation_easy(AA['#meetings'], choices=validation)


# # # OLDER INFORMATION # # #
@special_col
def format_voted_F22(wsheet):
    '''Voted (F22) col.'''
    with wsheet.batching(passthrough=True):
        wsheet.col_conditional_formatting('Voted (F22)', if_text='True', **TEXT_GOOD)

@special_col
def format_voted_SAV(wsheet):
    '''Voted (SAV) col.'''
    with wsheet.batching(passthrough=True):
        wsheet.col_conditional_formatting('Voted (SAV)', if_text='True', **TEXT_GOOD)


''' --------------------- Building Chart - Build Wall Sheet --------------------- '''

def rebuild_wall_sheet(wsheet, people, *, keys=None):
    '''clears google sheet then fills with wall chart data & appropriate formatting.
    wsheet: a WallChart object
        connected to the google sheet we are about to rebuild.
    people: a list of ThesisPerson objects
        all the people whose data should be put onto this sheet
    keys: None, 'STANDARD', or list of str
        if provided, use only these keys, instead of people[0].keys().
        if 'STANDARD', use keys=COLS_STANDARD.
    '''
    chart = build_chart(people, keys=keys)
    with wsheet.batching(passthrough=True):
        wsheet.clear_formats()
        write_chart_to_sheet(chart, wsheet)
        format_sheet_standard(wsheet)
        format_sheet_special(wsheet)
