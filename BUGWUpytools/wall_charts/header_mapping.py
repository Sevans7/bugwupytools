"""
File Purpose: HeaderMapping

map from one header to the other.
header is an iterable with string elements.

The wall chart headers may change over time,
which makes restoring from old data more challenging.
There are probably many columns with the same exact header,
but some may be added, removed, or altered slightly.
"""
from ..tools import (
    InputError,
    find,
    similarity,
    user_choice_numeric,
    format_docstring,
)


class HeaderMapping():
    '''mapping from one header to another.
    header is an iterable with string elements (inputs will be converted to str if necessary).

    the headers will be stored internally as tuples;
    self.header1 and self.header2 should not be altered after initialization.

    after doing some calculations, e.g. self.calculate_maps(), can use:
        self.map1to2[index in header1] <--> index in header2.
        self.map2to1[index in header2] <--> index in header1.
    '''
    # # # INITIALIZATION # # #
    def __init__(self, header1, header2, calculate_matches=False, **kw__calculate_matches):
        self.header1 = tuple(str(h) for h in header1)
        self.header2 = tuple(str(h) for h in header2)
        self._set_initial_unmatched()
        self._set_initial_maps()
        if calculate_matches:
            self.calculate_matches(**kw__calculate_matches)

    def _set_initial_unmatched(self):
        '''sets self.unmatchedN (N=1 or 2) to [(i, h) for (i, h) in enumerate(self.headerN)'''
        self.unmatched1 = list(enumerate(self.header1))
        self.unmatched2 = list(enumerate(self.header2))

    def _set_initial_maps(self):
        '''sets self.map2to1 and self.map1to2.
        Also some internal maps for bookkeeping.
        '''
        self.map1to2 = dict()   # map1to2[index in header1] <--> index in header2. 
        self.map2to1 = dict()   # map2to1[index in header2] <--> index in header1.
        self._trivial_map2to1 = dict()
        self._trivial_map1to2 = dict()
        self._nontrivial_map2to1 = dict()
        self._nontrivial_map1to2 = dict()

    # # # MAP BASICS # # #
    def _maps(self, h1index, h2index, trivial=True):
        '''tells self that header1[h1index] <--> header2[h2index].
        trivial: bool, default True
            for bookkeeping; also stores info in
            self._trivial_mapNtoM (if trivial) or self._nontrivial_mapNtoM (if not trivial)
        '''
        self.map1to2[h1index] = h2index
        self.map2to1[h2index] = h1index
        if trivial:
            self._trivial_map1to2[h1index] = h2index
            self._trivial_map2to1[h2index] = h1index
        else:
            self._nontrivial_map1to2[h1index] = h2index
            self._nontrivial_map2to1[h2index] = h1index

    def map_to_matches(self, map_, mode='1to2'):
        '''returns tuple of (self.header1[key], self.header2[val]) for key, val in map_.items().
        mode: '1to2' or '2to1'
            '1to2' --> returns tuple of elements (self.header1[key], self.header2[val])
            '2to1' --> returns tuple of elements (self.header2[key], self.header1[val])
        '''
        if mode=='1to2':
            return tuple((self.header1[key], self.header2[val]) for key, val in map_.items())
        elif mode=='2to1':
            return tuple((self.header2[key], self.header1[val]) for key, val in map_.items())
        else:
            raise InputError(f"Invalid mode={repr(mode)}; expected '1to2' or '2to1'.")

    # # # DISPLAY # # #
    def _maps_known_str(self):
        '''str for number of maps known'''
        LH1 = len(self.header1)
        LH2 = len(self.header2)
        L12 = len(self.map1to2)
        L21 = len(self.map2to1)
        return f'header1 to 2: {L12} / {LH1}; header2 to 1: {L21} / {LH2}'

    def __repr__(self):
        return f'{type(self).__name__}(maps known: {self._maps_known_str()})'

    # # # CALCULATE MATCHES # # #
    def calculate_trivial_matches(self, force=False):
        '''determines trivial matches between the headers; sets some attributes internally.
        "trivial match" if strings are exactly equal AND not empty.

        force: bool, default False
            if False, skip this operation if self._trivial_map1to2 or self._trivial_map2to1 have len > 0.

        trivial matches are put into self.map1to2 and self.map2to1.
        also into self._trivial_map1to2 and self._trivial_map2to1.

        self.unmatched1 and self.unmatched2 are updated to remove trivially-matched pairs.
        '''
        if (not force) and (len(self._trivial_map1to2)>0 or len(self._trivial_map2to1)>0):
            return   # skip; already did the calculation and we're not being forced to repeat it.
        unmatched1 = self.unmatched1   # list of (i1, val1), with header1[i1]==val1.
        unmatched2 = self.unmatched2   # list of (i2, val2), with header2[i2]==val2.
        def _matches(umA, umB):
            return (umA[1]!='') and (umA[1]==umB[1])
        l1 = 0  # loop index for 1
        while l1 < len(unmatched1):
            l2 = find(unmatched2, unmatched1[l1], equals=_matches)
            if l2 is None:  # no match
                l1 += 1
            else:  # found a match;  unmatched2[l2][1] == unmatched1[l1][1]
                i1, _val1 = unmatched1.pop(l1)
                i2, _val2 = unmatched2.pop(l2)
                self._maps(i1, i2, trivial=True)

    def _get_unmatched_similarity_scores(self, threshold=None):
        '''returns similarity scores for unmatched values in self.

        threshold: None or value between 0 (fully different) and 1 (exactly the same)
            if provided, only return scores which are at least equal to the threshold.

        ignores blank values, i.e. does not consider empty strings.
        
        returns {(l1, l2): similarity between unmatched1[l1][1] and unmatched2[l2][1]}
        '''
        umscores = dict()
        for l1, (_i1, h1str) in enumerate(self.unmatched1):
            if h1str=='': continue   # ignore empty strings
            for l2, (_i2, h2str) in enumerate(self.unmatched2):
                if h2str=='': continue   # ignore empty strings
                score = similarity(h1str, h2str)
                if (threshold is None) or (score >= threshold):
                    umscores[(l1, l2)] = score
        return umscores

    @staticmethod
    def _get_sorted_similarity_scores(umscores):
        '''returns sorted similarity scores list.'''
        scores_list = [(score, (l1, l2)) for (l1, l2), score in umscores.items()]
        return sorted(scores_list, key=lambda v: v[0], reverse=True)

    _potential_new_matches_paramdocs = \
        '''potential new match is a pair (val1 from header1, val2 from header2),
        with val1 and val2 not yet matched (not in self.unmatched1 or self.unmatched2).

        threshold: value from 0 to 1
            similarity threshold. 0 means fully different; 1 means exactly the same.'''

    @format_docstring(paramdocs=_potential_new_matches_paramdocs)
    def potential_new_matches(self, threshold):
        '''returns the potential new matches above the similarity threshold,
        in a human-readable way, i.e. with (score, string from header1, string from header2).

        {paramdocs}
        '''
        unmatched_scores = self._get_unmatched_similarity_scores(threshold=threshold)
        result = tuple((f'{score:.2f}', self.unmatched1[l1][1], self.unmatched2[l2][1])
                        for (l1, l2), score in unmatched_scores.items())
        return result

    @format_docstring(paramdocs=_potential_new_matches_paramdocs)
    def n_potential_new_matches(self, threshold):
        '''returns number of potential new matches above the similarity threshold.
        
        {paramdocs}
        '''
        return len(self._get_unmatched_similarity_scores(threshold=threshold))

    _nontrivial_matches_paramdocs = \
        '''choice_method: 'user' or 'auto'
            how to choose the result (only if an above-threshold similarity is found).
            'user' --> ask user to choose from above-threshold similarity options.
            'auto' --> pick the most-similar of the above-threshold similarity options.
        threshold: value between 0 and 1
            threshold for "similar enough to maybe be a match."
            similarity scoring is done by the tools.similarity method.
            if choice_method 'user', show user all options with similarity >= threshold.'''

    @format_docstring(paramdocs=_nontrivial_matches_paramdocs)
    def calculate_nontrivial_matches(self, choice_method='user', threshold=0.7):
        '''determine nontrivial matches between the headers; sets some attributes internally.
        if found any matches, adjusts:
            self.unmatched1, self.unmatched2,
            self.map2to1, self.map1to2,
            self._nontrivial_map2to1, self._nontrivial_map1to2.

        Note: makes those adjustments after finishing the loop;
            if there is a crash in the middle (e.g. if user enters 'q'), makes no changes.
        
        {paramdocs}

        returns list of found matches, as (string from header 1, string from header 2) pairs. 
        '''
        found = []
        dict_scores = self._get_unmatched_similarity_scores(threshold=threshold)  # key (l1, l2); value score

        # loop, so we can do multiple choices
        while len(dict_scores)>0:
            sorted_scores = self._get_sorted_similarity_scores(dict_scores)
            # else, decide i_choice, corresponding to the match to do.
            if choice_method == 'auto':
                i_choice = 0
            elif choice_method == 'user':
                message = 'Found 1 or more "close matches". Which of these are definitely correct?:'
                score_strs = [f'{score:.3f}' for score, _l1l2 in sorted_scores]
                option_strs = [f'({repr(self.unmatched1[l1][1])}) <--> ({repr(self.unmatched2[l2][1])})'
                                for score, (l1, l2) in sorted_scores]
                STRS_IF_WRONG = ('n', 'none', 'N', 'NONE', 'None')
                i_choice = user_choice_numeric(option_strs,
                        message=message,
                        scores=score_strs,
                        score_word='similarity',
                        secret_options=STRS_IF_WRONG,
                        extra_prompt=" (or 'none' if they are all bad or you're not sure)",
                        )
                if i_choice in STRS_IF_WRONG:
                    break  # user says everything remaining is wrong.
            else:
                errmsg = f"Got invalid choice_method: {repr(choice_method)}; expected 'user' or 'auto'"
                raise InputError(errmsg)
            # if we made it to this line, it means a choice has been made.
            choice = sorted_scores[i_choice]
            _score, (l1, l2) = choice
            found.append((l1, l2))   # track -- we found this match! will handle after loop.
            # remove l1 and l2 from dict_scores; we don't need to match either of them anymore.
            for key in list(dict_scores.keys()):
                if key[0]==l1 or key[1]==l2:
                    del dict_scores[key]
        
        # all choices have been made and are now in found.
        # now we handle the choices (after that loop ^ in case of crash during loop!)
        result = []
        # mapping
        for (l1, l2) in found:
            self._maps(self.unmatched1[l1][0], self.unmatched2[l2][0], trivial=False)
            result.append((self.unmatched1[l1][1], self.unmatched2[l2][1]))
        # removing from self.unmatched
        l1_found, l2_found = zip(*found) if len(found)>0 else ((),())
        for l1pop in sorted(l1_found, reverse=True):  # highest first --> pop won't mess up the indexing.
            self.unmatched1.pop(l1pop)
        for l2pop in sorted(l2_found, reverse=True):  # highest first --> pop won't mess up the indexing.
            self.unmatched2.pop(l2pop)
        # result
        return result

    @format_docstring(paramdocs=_nontrivial_matches_paramdocs)
    def calculate_matches(self, choice_method='user', threshold=0.7, verbose=False):
        '''calculates matches in self; returns self.map_to_matches(self.map1to2).
        To actually use the result, it is easier to use self.map1to2 (or self.map2to1).

        Handling of non-trivial matches (i.e. not exactly equal) is controlled by params:
        {paramdocs}

        More params:
        verbose: bool, default False
            if True, print nontrivial matches.
        '''
        _tm = self.calculate_trivial_matches()
        _ntm = self.calculate_nontrivial_matches(choice_method=choice_method, threshold=threshold)
        if verbose and len(_ntm)>0:
            print('Found nontrivial matches:', _ntm)
        return self.map_to_matches(self.map1to2)
