"""
File Purpose: WallChart class.

a WallChart is a Worksheet containing info from one department.
a WallDoc is a Spreadsheet, possibly containing multiple WallCharts.
"""
from .wall_chart_data import (
    WallChartData,
)
from ..tools import (
    SheetsChart, Chart,
    df_ndiff_rows_and_cells,
    format_docstring,
)

class WallChart(SheetsChart):
    '''a Worksheet containing info from one department.
    Defines high-level methods for saving, restoring, and editing data.

    contains methods specific to BUGWU wall charts.
    Also designed to be a bit robust (e.g. "restore" option) in case of bugs or mistakes.
    '''
    def as_data(self, reread=False, **kw__from_wallchart):
        '''returns WallChartData representing the data in self at this time.
        reread: bool, default False
            True --> read the data from the google sheet again, and update cached result in self.
            False --> use cached result from self, if it exists.
        '''
        if not reread:
            try:
                return self._cached_as_data
            except AttributeError:
                pass  # handled below
        result = WallChartData.from_wallchart(self, **kw__from_wallchart)
        self._cached_as_data = result
        return result

    def backup(self, dir=None, dryrun=False, **kw__to_excel):
        '''saves a backup of the data from self,
        in the directory provided, or wall_chart_defaults.DEFAULT_SAVEDIR if dir is None.

        dryrun: bool, default False
            if True, don't actually backup anything! But still return the same thing.

        returns (WallChartData of data from self, (abspath to saved file, sheet name))
        '''
        wcd = self.as_data(keep_formulas=True)  # wcd <--> "wall_chart_data"
        return (wcd, wcd.to_excel(dir=dir, dryrun=dryrun, **kw__to_excel))

    def overwrite(self, arr, dryrun=False, filter=(1,1), **kw__backup):
        '''overwrite worksheet using values from arr.
        Equivalent to clearing the worksheet (empty all cells), then filling with arr.
        Before doing this operation, saves a backup to local machine, via self.backup.

        dryrun: bool, default False
            if True, don't actually overwrite anything, and don't backup anything, either!
            (but do still return ndiffs)
        filter: bool, 'clear', or something which can imply a CellRangeIndex
            whether/how to update simple filter. see self.update_values() docs for help.

        returns ((number of changed rows, number of changed cells),
                 (abspath to backup file, sheet name in backup file))
        '''
        if not isinstance(arr, Chart):
            arr = Chart(arr)
        # backup & bookkeeping
        wcd, backup = self.backup(dryrun=dryrun, **kw__backup)
        ndiffs = self._internal_call__overwrite(arr, self_wcd=wcd, dryrun=dryrun, filter=filter)
        return (ndiffs, backup)

    def _internal_call__overwrite(self, chart, *, self_wcd, dryrun=True, filter=(1,1)):
        '''overwrite worksheet using values from chart. self_wcd is to calculate ndiffs.
        NOT FOR END-USER -- for internal call, only.
        DOES NOT BACK UP FIRST. USE WITH CAUTION.

        dryrun: bool, default True
            if dryrun, don't actually overwrite anything! Still return the same thing though.
            Set to True by default, for extra caution,
                so that accidental calls (which do not set dryrun=False) will not overwrite anything.
        filter: bool, 'clear', or something which can imply a CellRangeIndex
            whether/how to update simple filter. see self.update_values() docs for help.
            (Doesn't edit contents directly, but provides the option to sort by header.)

        returns (number of changed rows, number of changed cells)
        '''
        # calculate ndiffs
        ndiffs = self_wcd.ndiffs(chart)
        # smash the cells online; fill with df.
        if not dryrun:
            self.write_values_chart(chart, filter=filter)
        # return result
        return ndiffs

    def restore_from_backup(self, backupfile=None, backupsheet=None, dir=None, **kw__read):
        '''restores self from backup (an excel file on local machine).
        Note: this is equivalent to self.overwrite( DataFrame from the backup ).
        Hence, it will generate a new backup before performing the operation.

        if backupfile and backupsheet are not provided,
            make intelligent guesses (i.e.: self.spreadsheet.title, self.title)
        dir: where to look for the backup.
            if None, try wall_chart_defaults.DEFAULT_SAVEDIR.
            if still None, use current directory.

        returns result of self.overwrite( DataFrame from the backup ), i.e.:
                ((number of changed rows, number of changed cells),
                 (abspath to backup file, sheet name in backup file))
        '''
        # bookkeeping
        filename = self.spreadsheet.title if backupfile is None else backupfile
        sheet_name = self.title if backupsheet is None else backupsheet
        # get WallChartData
        wcd = WallChartData.from_excel(filename, sheet_name=sheet_name, dir=dir, **kw__read)
        # overwrite existing sheet using the data from backup.
        return self.overwrite(wcd)

    # # # HELPFUL PROPERTIES / METHODS # # #
    def _set_cached_data(self, chart):
        '''sets self._cached_as_data = chart.
        This is the value returned from self.as_data(reread=False).
        '''
        self._cached_as_data = chart

    header = property(lambda self: self.as_data(reread=False).header,
                      doc='''header of self.as_data(). Uses cached self.as_data() instead of rereading.''')

    def icol(self, label, reread=False):
        '''returns index of label in self.header, as int (0-indexed, and not numpy.int64).'''
        data = self.as_data(reread=reread)
        return int(data.icol(label))

    _col_data_range_docs = '''start_col: string, int, or None
            string --> starting column label ("label", meaning value in top row)
            int --> 1-indexed column (e.g., 3 for 'C')
            None --> use left-most column. Equivalent to start_col=1.
        end_col: string, int, or None
            string --> ending column label ("label", meaning value in top row)
            int --> 1-indexed column (e.g. 3 for 'C')
            None --> use same column as start if only='col' (default), else use right-most column.
        only: 'col' or None
            controls behavior for `end_col` when end_col is None (as described ^).
        rows: tuple, default (2, None)
            rows (1-indexed) to include. By default, row 2 through the bottom row.'''

    @format_docstring(_col_data_range_docs=_col_data_range_docs)
    def _col_data_range(self, start_col, end_col=None, only='col', rows=(2, None), **kw_data_range):
        '''returns ((start row, start col), (end row, end col)) (1-indexed).

        {_col_data_range_docs}
        '''
        if isinstance(start_col, str):
            start_col = self.icol(start_col) + 1  # +1 --> 1-indexed
        if isinstance(end_col, str):
            end_col = self.icol(end_col) + 1  # +1 --> 1-indexed
        start_row, end_row = rows
        start = (start_row, start_col)
        end   = (end_row,   end_col)
        result = self._data_range(start, end, only=only, **kw_data_range)
        return result

    @format_docstring(_col_data_range_docs=_col_data_range_docs)
    def col_data_validation(self, start_col, condition_type, condition_values=None, *,
                            end_col=None, only='col', rows=(2, None),
                            **kw__set_data_validation):
        '''sets data validation for cells in indicated column(s).

        for more help & more precise control over which cells to affect, see self.set_data_validation.
        
        {_col_data_range_docs}
        *args and **kwargs go to self.set_data_validation()
        '''
        start, end = self._col_data_range(start_col, end_col, only=only, rows=rows)
        return self.set_data_validation(start, condition_type=condition_type, condition_values=condition_values,
                                        end=end, only=only, **kw__set_data_validation)

    @format_docstring(_col_data_range_docs=_col_data_range_docs)
    def col_data_validation_easy(self, start_col, *, end_col=None, only='col', rows=(2, None),
                                 choices=None, bool_=None, **kw):
        '''sets data validation for the indicated cells, based on the relevant entered kwarg.
        for more options than these kwargs, use self.set_data_validation.

        for more help & more precise control over which cells to affect, see self.set_data_validation_easy.

        {_col_data_range_docs}
        **kw go to self.set_data_validation_easy
        '''
        start, end = self._col_data_range(start_col, end_col, only=only, rows=rows)
        return self.set_data_validation_easy(start, end=end, only=only, choices=choices, bool_=bool_, **kw)

    @format_docstring(_col_data_range_docs=_col_data_range_docs)
    def col_conditional_formatting(self, start_col, end_col=None, color=None, *, only='col', rows=(2, None),
                                   if_text=None, if_text_in_col=None, **kw):
        '''sets conditional formatting for indicated columns, to color cells based on rule.

        for more precise control over formatting, consider self.add_conditional_formatting_easy.

        {_col_data_range_docs}
        color: None or string representing a color (e.g. "red" or a hex value)
            background color for cells with this formatting.
            Use: {{'backgroundColorStyle': {{'rgbColor': dict corresponding to this color}}}}
            string can be hex or any color recognized by matplotlib.colors.to_rgb.
        if_text: None or string
            if provided, use 'TEXT_EQ' formatting, for text matching this value exactly.
        if_text_in_col: None or tuple (text_to_match, col)
            if provided, use 'CUSTOM_FORMULA' formatting, for rows where text in col matches text_to_match.
            E.g. ("matchme", 'label') and (value in C1=="label") --> formula= '=$C2="matchme"'
            col can be string (e.g. 'label') or 1-indexed integer (e.g. 3 corresponds with 'C').
        '''
        start, end = self._col_data_range(start_col, end_col, only=only, rows=rows)

        if if_text_in_col is not None:
            text_to_match, col = if_text_in_col
            if isinstance(col, str):
                col = self.icol(col) + 1  # +1 --> 1-indexed
            if_text_in_col = (text_to_match, col)

        return self.add_conditional_formatting_easy(start, end, color=color, only=only,
                                                    if_text=if_text, if_text_in_col=if_text_in_col, **kw)

    @format_docstring(_col_data_range_docs=_col_data_range_docs)
    def col_apply_format(self, start_col, format_info, *, end_col=None, only='col', rows=(2, None), **kw):
        '''sets formatting for indicated columns.
        format_info: dict, or single pygsheets cell
            dict --> properties specifying the formats to be updated, for details see
                https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#CellFormat
            pygsheets cell --> format all indicated cells to match the formatting of this cell.

        for more precise control over which cells to format, consider self.apply_format.

        {_col_data_range_docs}
        '''
        cdr = self._col_data_range(start_col, end_col, only=only, rows=rows)
        return self.apply_format(cdr, format_info, **kw)

    def col_adjust_widths_to_fit_header(self, min_pixels=100, **kw):
        '''adjusts widths of cols in self to fit text in header.
        [TODO] make this better...
        in the meantime, adjust manually, then use self.get_column_widths,
        then can use self.set_column_widths later.
        '''
        with self.batching(passthrough=True):
            for i, htext in enumerate(self.header):
                self.adjust_column_width_to_fit_header(i+1, htext, min_pixels=min_pixels, **kw)

    def get_header_widths(self):
        '''returns dict of {label in top row: width of that column (in pixels)}.
        Skips blank cells.
        '''
        widths = self.get_column_widths()
        header = self.header
        result = {key: w for key, w in zip(header, widths) if key!=''}
        return result

    def set_header_widths(self, new_widths):
        '''sets widths of columns based on dict of new widths.
        keys should be in header.
        '''
        with self.batching(passthrough=True):
            for key, width in new_widths.items():
                i = self.icol(key) + 1   # +1 accounts for 1-indexing.
                self.adjust_column_width(i, pixel_size=width)
