"""
File Purpose: WallDoc class.

a WallChart is a Worksheet containing info from one department.
a WallDoc is a Spreadsheet, possibly containing multiple WallCharts.

[TODO] dryrun option? E.g. learn what will be changed before actually changing it.
[TODO] read entire google sheets doc in one call, to reduce read requests to google sheets API?
"""

from .wall_chart import (
    WallChart,
)
from .wall_doc_data import (
    WallDocData,
)
from ..tools import (
    SheetsDoc,
    open_spreadsheet,
    format_docstring,
)
from ..tools.pygsheets_tools.custom_pygsheets import _open_spreadsheet_paramdocs

EXCLUDE_SHEETS = ['Tally', 'tally']


''' --------------------- open WallDoc --------------------- '''

@format_docstring(paramdocs=_open_spreadsheet_paramdocs)
def open_walldoc(client, key=None, *, title=None, url=None, as_json=None):
    '''opens spreadsheet (a google sheets doc); returns WallDoc object.

    {paramdocs}
    '''
    spreadsheet = open_spreadsheet(client, key=key, title=title, url=url, as_json=as_json)
    return WallDoc(spreadsheet)


''' --------------------- WallDoc class --------------------- '''

class WallDoc(SheetsDoc):
    '''a Spreadsheet containing info from possibly multiple departments.
    Defines high-level methods for saving, restoring, and editing data.

    contains methods specific to BUGWU wall charts.
    Also designed to be a bit robust (e.g. "restore" option) in case of bugs or mistakes.
    '''
    worksheet_cls = WallChart   # "my worksheets are WallCharts."

    def as_data(self, sheets=None, exclude_sheets=EXCLUDE_SHEETS, **kw__from_walldoc):
        '''returns WallDocData representing the data in self at this time.
        == WallDocData.from_walldoc(self, sheets=sheets, exclude_sheets=exclude_sheets).
        '''
        return WallDocData.from_walldoc(self, sheets=sheets, exclude_sheets=exclude_sheets, **kw__from_walldoc)

    def backup(self, dir=None, sheets=None, *, exclude_sheets=EXCLUDE_SHEETS, dryrun=False, keep_formulas=True, **kw__to_excel):
        '''saves a backup of the data from self,
        in the directory provided, or wall_chart_defaults.DEFAULT_SAVEDIR if dir is None.

        dryrun: bool, default False
            if dryrun, don't backup anything! But still return the same thing.

        returns (WallDocData of data from self, abspath to saved file)
        '''
        wdd = self.as_data(sheets=sheets, exclude_sheets=exclude_sheets, keep_formulas=keep_formulas)  # wdd <--> "wall_doc_data"
        return (wdd, wdd.to_excel(dir=dir, dryrun=dryrun, **kw__to_excel))

    def overwrite_worksheets(self, wdd, *, dryrun=False, filter=(1,1), **kw__backup):
        '''overwrite worksheets using values from wdd, probably a WallDocData object.
        Only alters worksheets with same title as worksheets in wdd.
        All worksheets from wdd must be present in self, else will crash before doing anything.

        wdd: WallDocData or dict
            keys should be sheet titles; values should be WallChartData objects
        dryrun: bool, default False
            if dryrun, don't overwrite anything! But still return the same thing.
        filter: bool, 'clear', or something which can imply a CellRangeIndex
            whether/how to update simple filter on each sheet. see WallChart.update_values() docs for help.
            (Doesn't edit contents directly, but provides the option to sort by header.)

        returns (ndiffs total, dict of ndiffs for each sheet, abspath to backup file),
            where ndiffs is a tuple with (number of changed rows, number of changed cells).
        '''
        # backup & bookkeeping
        wallcharts_dict = self.sheets_dict(sheets=wdd.keys())  # (might crash, appropriately though.) 
        self_wdd, backup = self.backup(dryrun=dryrun, **kw__backup)
        # overwrite each worksheet
        ndiffs_total = [0, 0]
        ndiffs_dict = {}
        for sheet_name, wall_chart_data in wdd.items():
            wallchart = wallcharts_dict[sheet_name]
            ndiffs = wallchart._internal_call__overwrite(wall_chart_data, self_wcd=self_wdd[sheet_name],
                                                         dryrun=dryrun, filter=filter)
            ndiffs_dict[sheet_name] = ndiffs
            ndiffs_total[0] += ndiffs[0]
            ndiffs_total[1] += ndiffs[1]
        return (ndiffs_total, ndiffs_dict, backup)

    def restore_from_backup(self, backupfile=None, dir=None, **kw__read):
        '''restores self from backup (an excel file on local machine).
        Note: this is equivalent to self.overwrite_worksheets( WallDocData from the backup ).
        Hence, it will generate a new backup before performing the operation.

        if backupfile not provided,
            make an intelligent guess, i.e.: self.title
        dir: where to look for the backup.
            if None, try wall_chart_defaults.DEFAULT_SAVEDIR.
            if still None, use current directory.

        returns result of self.overwrite_worksheets( WallDocData from the backup ), i.e.:
            (ndiffs total, dict of ndiffs for each sheet, abspath to backup file)
        '''
        # bookkeeping
        filename = self.title if backupfile is None else backupfile
        # get WallDocData
        wdd = WallDocData.from_excel(filename, dir=dir, **kw__read)
        # overwrite existing sheets using the data from backup.
        return self.overwrite_worksheets(wdd)


''' --------------------- WallChart bookkeeping --------------------- '''

# tell WallCharts that their spreadsheet parents would be WallDoc objects.
WallChart.spreadsheet_cls = WallDoc