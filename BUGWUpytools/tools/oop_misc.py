"""
File Purpose: Miscellaneous quality-of-life functions for Object Oriented Programming tasks.
"""

import functools

TRACEBACKHIDE = lambda: True   # whether to hide some things from tracebacks.


''' --------------------- Alias an attribute --------------------- '''

def alias(attribute_name, doc=None):
    '''returns a property which is an alias to attribute_name.
    if doc is None, use doc=f'alias to {attribute_name}'.
    '''
    return property(lambda self: getattr(self, attribute_name),
                    doc=f'''alias to {attribute_name}''' if doc is None else doc)

def alias_to_result_of(attribute_name, doc=None):
    '''returns a property which is an alias to the result of calling attribute_name.
    if doc is None, use doc=f'alias to {attribute_name}()'.
    '''
    return property(lambda self: getattr(self, attribute_name)(),
                    doc=f'''alias to {attribute_name}()''' if doc is None else doc)

def alias_in(cls, attr, new_attr):
    '''sets cls.new_attr to be an alias for cls.attr.'''
    setattr(cls, new_attr, alias(attr))


''' --------------------- Caching --------------------- '''

def caching_attr_simple_if(cache_if, attr_name=None, cache_fail_ok=False):
    '''returns a function decorator for f(x, *args, **kw) which does caching if cache_if().
    cache_if: callable of 0 arguments
        attempt caching if cache_if().
    attr_name: None or string
        which attribute (of x) to cache to.
        None --> use '_cached_{f.__name__}'
    cache_fail_ok: bool, default False
        whether it is okay to fail to store the cached info, if caching.
        (e.g. if cache_if() and x is a tuple, raise AttributeError unless fail_ok=True)
    '''
    def f_with_caching_attr_simple_if(f):
        '''function decorator for f but first checks cache if cache_if().'''
        cache_attr_name = attr_name if attr_name is not None else f'_cached_{f.__name__}'
        @functools.wraps(f)
        def f_but_maybe_caching(x, *args, **kw):
            '''does f, but does caching if cache_if()'''
            __tracebackhide__ = TRACEBACKHIDE()
            caching = cache_if()
            if caching:
                try:
                    return getattr(x, cache_attr_name)
                except AttributeError:
                    pass  # result not yet cached.
            result = f(x, *args, **kw)
            if caching:
                try:
                    setattr(x, cache_attr_name, result)
                except AttributeError:  # can't set attr, e.g. maybe self is a tuple.
                    if not cache_fail_ok:
                        raise AttributeError(f'failed to set x.{cache_attr_name} for type(x)={type(x)}.') from None
            return result
        return f_but_maybe_caching
    return f_with_caching_attr_simple_if


''' --------------------- Maintain Attributes (context manager) --------------------- '''

def maintain_attrs(*attrs):
    '''return decorator which restores attrs of obj after running function.
    It is assumed that obj is the first arg of function.
    '''
    def attr_restorer(f):
        @functools.wraps(f)
        def f_but_maintain_attrs(obj, *args, **kwargs):
            '''f but attrs are maintained.'''
            __tracebackhide__ = TRACEBACKHIDE()
            with MaintainingAttrs(obj, *attrs):
                return f(obj, *args, **kwargs)
        return f_but_maintain_attrs
    return attr_restorer

class MaintainingAttrs():
    '''context manager which restores attrs of obj to their original values, upon exit.'''
    def __init__(self, obj, *attrs):
        self.obj = obj
        self.attrs = attrs

    def __enter__(self):
        self.memory = dict()
        for attr in self.attrs:
            if hasattr(self.obj, attr):
                self.memory[attr] = getattr(self.obj, attr)

    def __exit__(self, exc_type, exc_value, traceback):
        for attr, val in self.memory.items():
            setattr(self.obj, attr, val)


def with_attrs(**attrs_and_values):
    '''return decorator which sets attrs of object before running function then restores them after.
    It is assumed that obj is the first arg of function.
    '''
    def attr_setter_then_restorer(f):
        @functools.wraps(f)
        def f_but_set_then_restore_attrs(obj, *args, **kwargs):
            '''f but attrs are set beforehand then restored afterward.'''
            __tracebackhide__ = TRACEBACKHIDE()
            with MaintainingAttrs(obj, *attrs_and_values.keys()):
                for attr, value in attrs_and_values.items():
                    setattr(obj, attr, value)
                return f(obj, *args, **kwargs)
        return f_but_set_then_restore_attrs
    return attr_setter_then_restorer
