"""
File Purpose: tools whose main purpose is to work with implementation details of Python.

E.g. manipulate function docstrings
"""
import inspect

from .errors import InputError, InputMissingError

def format_docstring(*args__format, **kw__format):
    '''returns a decorator of f which returns f, after updating f.__doc__ via f.__doc__.format(...)'''
    def return_f_after_formatting_docstring(f):
        f.__doc__ = f.__doc__.format(*args__format, **kw__format)
        return f
    return return_f_after_formatting_docstring

def value_from_aliases(*aliases):
    '''returns value from aliases.
    Precisely one of the aliases must be non-None, else raises InputError.
    '''
    not_nones = [(a is not None) for a in aliases]
    Nvals     = sum(not_nones)
    if Nvals == 1:
        return aliases[next(i for (i, not_none) in enumerate(not_nones) if not_none)]
    else:
        raise InputError(f'Expected one non-None value, but got {Nvals}!')

def assert_values_provided(**kw):
    '''raise InputMissingError if any of the kwargs' values are None.'''
    for key, val in kw.items():
        if val is None:
            raise InputMissingError(f'Must provide a non-None value for kwarg {repr(key)}.')

def printsource(obj):
    '''prints source code for object (e.g. call this on a function or class).'''
    print(inspect.getsource(obj))

def inputs_as_dict(callable_, *args, **kw):
    '''returns dict of all inputs to callable_ based on its signature and args & kwargs.
    raises TypeError if inputs would be invalid for callable_.
    Example:
        def foo(a, b=2, c=3, * d=4, e=5): pass
        inputs_as_dict(foo, 9, d=7, c=8) gives {'a':9, 'b':2, 'c':8, 'd':7, 'e':5}
        inputs_as_dict(foo, z=6) raises TypeError since foo doesn't accept kwarg 'z'.
    '''
    _iad_for_callable = _inputs_as_dict__maker(callable_)
    return _iad_for_callable(*args, **kw)

def _inputs_as_dict__maker(callable_):
    '''returns a function which returns dict of all inputs all inputs to callable_.'''
    f_signature = inspect.signature(callable_)
    def _inputs_as_dict(*args, **kw):
        '''returns dict of inputs as they would be named inside callable_'s namespace.
        includes params not input directly here, but defined by default for callable_.
        '''
        bound_args = f_signature.bind(*args, **kw)  # << will raise TypeError if inputs invalid for f.
        bound_args.apply_defaults()  # << include defaults
        params_now = bound_args.arguments  # << dict of {input name: value}.
        return params_now
    return _inputs_as_dict
