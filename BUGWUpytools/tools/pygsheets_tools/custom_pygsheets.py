"""
File Purpose: custom additions to pygsheets classes.

Note about naming convention:
    SheetsChart <--> pygsheets Worksheet. A single tab on a sheets document.
    SheetsDoc <--> pygsheets Spreadsheet. A whole sheets document.

Example:
    client = pygsheets.authorize(...)
    spreadsheet = open_spreadsheet(client, title=filename)

    Then the custom methods will be available, as well as the pygsheets methods.
    And getting a Worksheet from the spreadsheet will give a SheetsChart instead.

    Other options include open_spreadsheet(client, key=key)  # can find key in url
    also, open_spreadsheet(client, url=url).

[TODO] "addConditionalFormatRule": { "rule": { "gradientRule": ... }}
"""

from ..imports import ImportFailed

try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')

import pygsheets as pg   # this one is required for ANY functionality here to work.
Spreadsheet = pg.spreadsheet.Spreadsheet
Worksheet = pg.worksheet.Worksheet

from .pygsheets_tools_tools import (
    is_Exception__no_conditional_format_found,
    hex_to_rgb_dict,
    Batching,
    headersize_pixels,
)

from ..arrays import (
    array_to_list_of_lists, list_of_lists_to_array,
    df_to_list_of_lists,
)
from ..charts import Chart, CellIndex, CellRangeIndex
from ..errors import (
    InputMissingError, InputConflictError, InputError,
    PatternError,
)
from ..oop_misc import alias, alias_to_result_of
from ..pandas_tools import (
    df_reshape,
    df_write_to_excel,
)
from ..pytools import (
    format_docstring,
)

VALUE_RENDER = pg.ValueRenderOption.FORMULA  # or .FORMATTED_VALUE or .UNFORMATTED_VALUE


''' --------------------- open custom spreadsheet --------------------- '''

_open_spreadsheet_paramdocs = \
    '''client: pygsheets Client object
        e.g. from pygsheets.authorize(...)
    key: None or str
        if provided, client.open_by_key(key)
    title: None or str
        if provided, client.open(key)
    url: None or str
        if provided, client.open_by_url(url)
    as_json: None or str
        if provided, client.open_as_json(as_json)'''

@format_docstring(paramdocs=_open_spreadsheet_paramdocs)
def open_spreadsheet(client, key=None, *, title=None, url=None, as_json=None):
    '''opens spreadsheet; returns Spreadsheet object.

    {paramdocs}
    '''
    kw = dict(key=key, title=title, url=url, as_json=as_json)
    provided = tuple(k for k, v in kw.items() if v is not None)
    if len(provided) == 0:
        raise InputMissingError('key, title, url, or as_json must be provided.')
    elif len(provided) >= 2:
        raise InputConflictError(f'must provide exactly 1 value, but got: {provided}')
    # now, actually do the method.
    if key is not None:
        return client.open_by_key(key)
    elif title is not None:
        return client.open(title)
    elif url is not None:
        return client.open_by_url(url)
    elif as_json is not None:
        return client.open_as_json(as_json)
    else:
        assert False, "above handles all cases -- coding error if this line is reached."

@format_docstring(paramdocs=_open_spreadsheet_paramdocs)
def open_sheets_doc(client, key=None, *, title=None, url=None, as_json=None):
    '''opens spreadsheet; returns SheetsDoc object.
    Equivalent to SheetsDoc.from_client(client, ...)

    {paramdocs}
    '''
    return SheetsDoc.from_client(client, key=key, title=title, url=url, as_json=as_json)


''' --------------------- SheetsChart --------------------- '''

_filter_paramdocs = '''filter: bool, 'clear', or anything that can imply a CellIndex or CellRangeIndex.
            False --> do not adjust filter in any way.
            True --> set_basic_filter to contain all values
            'clear' --> clear_basic_filter(), i.e. remove any existing filter.
            other --> determine the implied CellIndex or CellRangeIndex.
                    (see CellIndex.implied_from & CellRangeIndex.implied_from for help.)
                    if a CellIndex (i.e. for a single cell), use that cell as the start, and use end=None.
                    if a CellRangeIndex, use those cells for the start & end.
                    1-indexed: if providing ints, use 1-indexing, e.g. (1,3) --> 'C1'.'''

class SheetsChart(Worksheet):
    '''pygsheets Worksheet with custom methods attached.
    "worksheet" is one sheet from a Spreadsheet.
    (Spreadsheet which is an entire sheets file.)
    
    initialize by entering a pygsheets worksheet object, or using the standard init for Worksheet.
    Or, use SheetsChart.from_client(...).
    '''
    def __init__(self, wks_or_arg, *args, **kw):
        self.kw__get_df = dict()
        if isinstance(wks_or_arg, Worksheet):
            self.wks = wks
            Worksheet.__init__(self, wks.spreadsheet, wks.jsonSheet, **kw)
        else:
            Worksheet.__init__(self, wks_or_arg, *args, **kw)
    
    @classmethod
    @format_docstring(paramdocs=_open_spreadsheet_paramdocs)
    def from_client(cls, client, key=None, sheet=0, *, title=None, url=None, as_json=None):
        '''create SheetsChart from client and info about the google doc to open.

        sheet: int or string, default 0
            which sheet to get from the Spreadsheet which client opens.
            0 --> get first sheet.
            string --> get sheet with that name.
        {paramdocs}
        '''
        spread = cls.spreadsheet_cls.from_client(client, key=key, title=title, url=url, as_json=as_json)
        return spread[sheet]

    def __repr__(self):
        return f'{type(self).__name__}(title={repr(self.title)}, index={self.index}, shape={self.shape})'

    # # # BATCHING # # #
    @property
    def batch_mode(self):
        '''alias to self.client.sheet.batch_mode.
        When True, updates will be batched locally.
        When False, updates will apply immediately to online sheet.
        To run all locally batched updates, use self.run_batch().

        CAUTION: batching is handled by self.client,
            which will be shared amongst all sheets opened with self.client as their client.
            Setting batch_mode for self will set batch_mode for those sheets, too.
            And run_batch() might involve any batched updates made to those sheets, too.
        '''
        return self.client.sheet.batch_mode
    @batch_mode.setter
    def batch_mode(self, value):
        '''sets self.batch_mode to value. Does NOT edit self.client.sheet.batched_requests;
        also does NOT run batched updates. Use self.run_batch() to run updates.
        '''
        self.client.sheet.batch_mode = value

    def run_batch(self, verbose=False, **kw):
        '''runs all batched updates in self.client. Then, clears self.client.sheet.batched_requests.
        CAUTION: this may involve updates to other sheets as well;
            will include ALL updates to any sheets made during client.sheet.batch_mode=True.
        Equivalent: self.client.sheet.run_batch(verbose=verbose, **kw).
        '''
        return self.client.sheet.run_batch(verbose=verbose, **kw)

    def clear_batch(self):
        '''clears batched requests in self.client, without running them first.
        CAUTION: batching is handled by self.client,
            which will be shared amongst all sheets opened with self.client as their client.
            clearing the batch will clear any batched updates to those sheets as well.
        '''
        self.client.sheet.batched_requests = dict()

    def batching(self, passthrough=True):
        '''returns a context manager for batch updates for a SheetsChart.

        behavior depends on passthrough & self.batch_mode on entry.
        if (not passthrough) or (self.batch_mode=False on entry):
            (on entry) clears batch. Set self.batch_mode=True
            (on exit) run batch if exiting without error, else clear batch. Set self.batch_mode=False
        otherwise (passthrough and self.batch_mode=True),
            do nothing. (Useful if may be batching() inside another batching() call;
                allows external batching() to control batching while interal call does nothing.)

        Note: batching is handled by client, not the sheet,
            so it's best to only work on one sheet per `with` block.

        Example:
            sheet = ...  # SheetsChart instance (possibly a subclass of SheetsChart)
            with sheet.batching():
                sheet.write_values_chart(...)
                sheet.set_data_validation(...)
                sheet.add_conditional_formatting_easy(*formatting_params_1)
                sheet.add_conditional_formatting_easy(*formatting_params_2)
            # This example writes values, sets data validation, and adds two conditional formatting rules.
            # The updates will all be applied during a single request to the API.
        '''
        return Batching(self, passthrough=passthrough)

    # # # GET VALUES # # #
    shape = property(lambda self: (self.rows, self.cols), doc='''(self.rows, self.cols)''')
    nrow = property(lambda self: self.rows, doc='''alias to self.rows. Number of rows in sheet.''')
    ncol = property(lambda self: self.cols, doc='''alias to self.cols. Number of cols in sheet.''')

    def get_array_shape(self):
        '''returns (nrow, ncol) required to contain all nonempty cells starting at top left.
        E.g. if data in C7, D2, and all else blank, returns (7, 4).

        [EFF] note: requires reading the entire sheet of data.
            But faster than self.get_array().shape; doesn't create numpy array.
        '''
        kw_empty = dict(include_tailing_empty=False, include_tailing_empty_rows=False)
        values = self.get_values((1,1), self.shape, value_render=VALUE_RENDER, **kw_empty)
        nrow = len(values)
        ncol = max(len(row) for row in values)
        return (nrow, ncol)
    
    def get_array(self, remove_empty=True, *, keep_formulas=True, **kw_array):
        '''returns array of values from self, removing all tailing empty rows & column.
        shape will be (number of nonempty rows, number of nonempty cols)
        
        remove_empty: if False, instead keep all rows and columns, and shape will be self.shape.
        keep_formulas: whether to keep formulas. If False, use data from formulas instead.
        
        additional kwargs are passed to np.array()
        '''
        if remove_empty:
            kw_empty = dict(include_tailing_empty=False, include_tailing_empty_rows=False)
        else:
            kw_empty = dict(include_tailing_empty=True, include_tailing_empty_rows=True)
        render = pg.ValueRenderOption.FORMULA if keep_formulas else pg.ValueRenderOption.FORMATTED_VALUE
        values = self.get_values((1, 1), self.shape, value_render=render, **kw_empty)  # << actually get the values
        if remove_empty:  # (must handle: each row might be a different length.)
            return list_of_lists_to_array(values, fill_value='', **kw_array)
        else:
            return np.array(values, **kw_array)

    def get_chart(self, remove_empty=True, **kw_get_array):
        '''returns Chart of values from self, removing all tailing empty rows & column.
        shape will be (number of nonempty rows, number of nonempty cols)
        if remove_empty=False, instead keep all rows and columns, and shape will be self.shape.
        
        additional kwargs are passed to np.array().
        equivalent to Chart(self.get_array(remove_empty, **kw_array))
        '''
        return Chart(self.get_array(remove_empty, **kw_get_array))

    as_data = alias('get_chart')

    def get_df(self, keep_formulas=True):
        '''return self.get_as_df(has_header=False).
        I.e. the top row of cells will be interpretted as data, not the header.
        keep_formulas: whether to keep formulas. If False, use data from formulas instead.
        '''
        render = pg.ValueRenderOption.FORMULA if keep_formulas else pg.ValueRenderOption.FORMATTED_VALUE
        return self.get_as_df(has_header=False, value_render=render)

    # # # UPDATE VALUES ONLINE # # #
    @format_docstring(filterdocs=_filter_paramdocs)
    def update_values(self, crange=(1,1), values=None, *, filter=False, **kw__super):
        '''update a range of cell values.
        crange: range (e.g. 'A1:C2'), or start cell (e.g. 'A1', (1,1)), default (1,1)
            cells to overwrite. (1-indexed)
            tuple --> (row, col).  (1-indexed).  E.g. (2,1) <--> 'A2'.
            single cell --> end cell will be inferred from values.
        values: list of list of values, or None
            matrix of values if range given, if a value is None it is unchanged.
            if None, skip updating values, but check for other updates, e.g. filter.
        {filterdocs}
        kw__super go to super().update_values.
        '''
        super().update_values(crange=crange, values=values, **kw__super)
        self.update_filter(filter, values=values, start=crange)

    def update_values_array(self, arr, start=(1,1), **kw__update_values):
        '''update values on the worksheet to match values in arr.
        start: upper left cell, default (1,1)
            Can be tuple or string, e.g. (2,3) <--> 'B3'
        '''
        values = array_to_list_of_lists(arr)
        self.update_values(start, values, **kw__update_values)

    update_values_chart = alias('update_values_array')

    @format_docstring(filterdocs=_filter_paramdocs)
    def write_values_chart(self, chart, filter=False, **kw__update_values):
        '''fill worksheet with values from chart; clear all other cells in the sheet.
        Note: equivalent to replacing all cells with chart.FILL, then
            setting the first r rows and c columns to the data from chart (with shape r, c).

        {filterdocs}
        '''
        chart_filled = chart.expand_shape_to_fit(self)
        result = self.update_values_chart(chart_filled, start=(1,1), filter=False, **kw__update_values)
        self.update_filter(filter, values=chart, start=(1,1))  # filter using the non-expanded chart!
        return result

    def update_values_df(self, df, start=(1,1), **kw__update_values):
        '''update values on the worksheet to match values in pandas DataFrame df.
        start: upper left cell, default (1,1)
            Can be tuple or string, e.g. (2,3) <--> 'B3'

        Note: only edits cells within shape of df:
            for df with shape (r, c), with start=(1,1),
            only adjusts the cells in range (row=1, col=1) to (row=1+r, col=1+c).
        '''
        values = df_to_list_of_lists(df)
        self.update_values(start, values, **kw__update_values)

    def write_values_df(self, df, fill_value='', **kw__update_values):
        '''fill worksheet with values from df; clear all other cells in the sheet.
        Note: equivalent to replacing all cells with fill_value, then
            setting the first r rows and c columns to the data from df (with shape (r, c)).
        '''
        df_filled = df_reshape(df, self.shape, fill_value=fill_value)
        return self.update_values_df(df_filled, start=(1,1), **kw__update_values)

    # # # WRITE TO EXCEL # # #
    def _get_default_dst_excel(self):
        '''returns default (filename, sheet name) for saving self.'''
        return (f'{self.spreadsheet.title}.xlsx', self.title)

    def write_to_excel(self, dst=None, sheet_name=None, **kw__write):
        '''saves wall chart to local machine at dst.
        if dst not provided, use worksheet name at file name.
            E.g. sheet 'Astronomy' from google doc 'CAS Sciences ABC'
            will save to local machine at sheet 'Astronomy' in file 'CAS Sciences ABC.xlsx'
        returns abspath of dst
        '''
        df = self.get_df()
        default_dst, default_sheet_name = self._get_default_dst_excel()
        if dst is None:
            dst = default_dst
        if sheet_name is None:
            sheet_name = default_sheet_name
        return df_write_to_excel(df, dst, sheet_name=sheet_name, **kw__write)

    # # # FORMAT STUFF ONLINE # # #
    @format_docstring(filterdocs=_filter_paramdocs)
    def update_filter(self, filter, values=None, start=None, dryrun=False):
        '''possibly updates filter. returns False if no update, else, filtered range as CellRangeIndex.
        if filter True, use start and values to determine filtered range (contain all values).
        {filterdocs}
        dryrun: bool, default False
            if True, don't update the filter; just return the range that would be filtered if dryrun=False.
        '''
        if not filter:
            return False
        if filter=='clear':
            if not dryrun: self.clear_basic_filter()
            return CellRangeIndex()  # << empty index; filter was cleared.
        if filter == True:
            if values is None:
                raise InputMissingError('values=None. values are required if filter==True.')
            frange = CellRangeIndex.implied_from(start, values, imode=1)
        else:
            frange = CellRangeIndex.implied_from(filter, imode=1)
        if not dryrun: self.set_basic_filter(start=frange.start1, end=frange.end1)
        return frange

    def set_buffer(self, nrows=50, ncols=5):
        '''sets the number of trailing empty rows and columns on the sheet online.
        
        This operation will add or remove rows & cols as necessary,
            but will only touch rows & cols which are blank (no changes to the data).
        '''
        assert nrows >= 0
        assert ncols >= 0
        rdat, cdat = self.get_array_shape()  # final row & col numbers containing any data
        self.resize(rows=rdat+nrows, cols=cdat+ncols)

    # # # DATA VALIDATION # # #
    _data_range_docs = '''start: None, string, or (row, col) tuple (1-indexed)
            determines cell address for start of range.
            string --> specify full cell address, or just col, or just row (1-indexed).
                        E.g. 'C2', 'C', '2'.
            tuple --> specify full cell address as (row, col), or just col, or just row (1-indexed).
                        E.g. (2, 3), (None, 3), (2, None)
            None --> use 'A1'.
            if row not provided, may use default value, depending on `only`.
                only == 'col' --> use row=_default_row_start.
                only == None or 'cell' --> raise PatternError
        end: None, string, or (row, col) tuple (1-indexed)
            determines cell address for end of range.
            string --> (same format as `start`), or '' to indicate no value provided (same as using None)
            tuple --> (same format as `start`), or (None, None) to indicate no value provided (same as using None)
            None --> row & col not specified; determined based on the value of `only`.
            if row not provided, value depends on `only`.
                only == 'cell' --> end_row = start row
                only == 'col' or None --> bottom row in self (==self.rows)
            if col not provided, value depends on `only`.
                only == 'cell' or 'col' --> end_col = start col
                only == None --> end_col = rightmost col in self (==self.cols)
        only: 'cell', 'col', or None.
            determines behavior for default values of row & col for start & end.
            see description of `start` & `end` above, for details.
        _default_row_start: int, default 2
            row to start at if only=='col' and start row not provided.
            default 2 avoids header row.'''

    @format_docstring(_data_range_docs=_data_range_docs)
    def set_data_validation(self, start, condition_type, condition_values=None, *,
                            end=None, only='cell', _default_row_start=2,
                            showCustomUi=True, strict=False, **kw__super):
        '''sets data validation for the cells implied by start and (end or only).

        condition_type: None or string
            refer to https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/other#conditiontype
            for possible inputs. Or use None to clear data validation for these cells.
        condition_values: list
            list of values, related to condition_type (see docs there).

        {_data_range_docs}
        showCustomUi: bool, default True
            whether to show dropdown menu in case of list-type validation.
        strict: bool, default False
            whether to do strict validation, i.e. reject invalid cell values.

        *args and **kw are passed to super().set_data_validation(start, end, *args, **kw).
        '''
        start, end = self._data_range(start, end, only, _default_row_start=_default_row_start)
        return super().set_data_validation(start, end, condition_type=condition_type, condition_values=condition_values,
                                           showCustomUi=showCustomUi, strict=strict, **kw__super)

    @format_docstring(_data_range_docs=_data_range_docs)
    def _data_range(self, start, end=None, only='cell', *, _default_row_start=2, as_cell_range_index=False, **kw__None):
        '''returns ((start row, start col), (end row, end col)) (1-indexed).

        {_data_range_docs}
        as_cell_range_index: bool, default False
            whether to return result as a CellRangeIndex (instead of a tuple)
            result_when_False equivalent to (result_when_True.start1, result_when_True.end1)
        '''
        _VALID_ONLY_VALS = ('cell', 'col', None)
        if only not in _VALID_ONLY_VALS:
            raise InputError(f'Invalid value for "only". Expected one of {_VALID_ONLY_VALS}; got only={repr(only)}')

        if start is None:
            start = 'A1'
        cstart = CellIndex.implied_from(start, imode=1)
        if cstart.row is None and only == 'col':
            cstart.row1 = _default_row_start

        if end is None:
            end = (None, None)
        cend = CellIndex.implied_from(end, imode=1)
        if cend.row is None:
            if only == 'cell':
                cend.row = cstart.row
            else:
                cend.row1 = self.rows  # bottom row in self
        if cend.col is None:
            if only == 'cell' or only == 'col':
                cend.col = cstart.col
            else:
                cend.col1 = self.cols  # rightmost col in self

        result_start = cstart.index1
        result_end = cend.index1
        if None in result_start or None in result_end:
            errmsg = (f'start and/or end not properly specified (got at least one None index).'
                      f'\nGot ((start row, start col), (end row, end col)) = {(result_start, result_end)}. '
                      f'\nInputs were: start={repr(start)}, end={repr(end)}, only={repr(only)}.')
            raise PatternError(errmsg)
        if as_cell_range_index:
            return CellRangeIndex.implied_from((cstart, cend))
        else:
            return (result_start, result_end)

    @format_docstring(_data_range_docs=_data_range_docs)
    def set_data_validation_easy(self, start, *, end=None, only='cell',
                                 choices=None, bool_=None, between=None, **kw):
        '''sets data validation for the indicated cells, based on the relevant entered kwarg.
        for more options than these kwargs, use self.set_data_validation.

        {_data_range_docs}

        DATA VALIDATION KWARGS (checked in this order; only the first provided value will matter):
            choices: None or list
                if provided, do "ONE_OF_LIST" validation. E.g. dropdown menu with values in list.
            bool_: None, True, or list
                if provided, do "BOOLEAN" validation. E.g. checkbox registering as TRUE or FALSE.
                if list, must be length 0, 1, or 2.
                    if len == 0, equivalent to using True. I.e., checkbox registers as TRUE or FALSE.
                    if len >= 1, box registers as bool_[0] when checked, '' when unchecked.
                    if len == 2, box registers as bool_[0] when checked, bool_[1] when unchecked.
            between: None or tuple (min, max)
                if provided, do "NUMBER_BETWEEN" validation, i.e. number between min and max.
                note: max is useful to avoid accidentally accepting dates.
                    e.g., typing "1-2" in a cell will be interpreted as the date 1/2/2023,
                    (or current year instead of 2023) which is 44928, as a number.
                the default help message using "NUMBER_BETWEEN" is also pretty helpful overall,
                    saying "Input must be a number between <min> and <max>".

        **kw go to self.set_data_validation
        '''
        # [TODO] custom data validation help message through google sheets API...
        if choices is not None:
            condition_type = "ONE_OF_LIST"
            condition_values = choices
        elif bool_ is not None:
            condition_type = "BOOLEAN"
            condition_values = None if (bool_==True) else bool_
        elif between is not None:
            condition_type = "NUMBER_BETWEEN"
            min_, max_ = between
            condition_values = [min_, max_]
        else:
            VALID_CHOICES = ('choices', 'bool_', 'between')
            raise InputMissingError(f"must provide one of the data validation kwargs: {VALID_CHOICES}")
        return self.set_data_validation(start, condition_type, condition_values, end=end, only=only, **kw)

    @format_docstring(_data_range_docs=_data_range_docs)
    def clear_data_validation(self, start=None, end=None, only=None, **kw):
        '''clears data validation rules for cells. (default: all cells in worksheet.)
        equivalent to self.set_data_validation(start, None, end=end, only=only, **kw)

        {_data_range_docs}
        '''
        return self.set_data_validation(start, None, end=end, only=only, **kw)

    # # # CONDITIONAL FORMATTING # # #
    def clear_conditional_formatting(self):
        '''clears ALL conditional formatting rules from this sheet.
        [TODO] delete only a subset of formatting rules.
        '''
        with self.batching(passthrough=True):
            sheet_formats = self.client.sheet.get(self.spreadsheet.id, ranges=[self.title], includeGridData=False)
            n = len(sheet_formats['sheets'][0].get('conditionalFormats', []))  # number of existing conditionalFormats on this sheet
            requests = [{"deleteConditionalFormatRule": {"index": str(i), "sheetId": self.id}}
                        for i in range(n)[::-1] ]  # [::-1] so we delete largest-index first, to avoid index relabeling.
            result = self.client.sheet.batch_update(self.spreadsheet.id, requests)
        return result

    @format_docstring(_data_range_docs=_data_range_docs)
    def add_conditional_formatting_easy(self, start, end=None, color=None, font_color=None, *, only='col',
                                        formula=None, if_text=None, if_text_in_col=None, **kw):
        '''sets conditional formatting, to color cells based on rule.
        "easy" because this just calls self.add_conditional_formatting(...)
        with things wrapped in the appropriate ways / slightly easier to use.
        
        {_data_range_docs}
        color: None or string representing a color (e.g. "red" or a hex value)
            background color for cells with this formatting.
            Use: dict(backgroundColorStyle=dict(rgbColor=dict corresponding to this color))
            string can be hex or any color recognized by matplotlib.colors.to_rgb.
        formula: None or string
            if provided, use 'CUSTOM_FORMULA' formatting, with this formula.
        if_text: None or string
            if provided, use 'TEXT_EQ' formatting, for text matching this value exactly.
        if_text_in_col: None or tuple (text_to_match, col)
            if provided, use 'CUSTOM_FORMULA' formatting, for rows where text in col matches text_to_match.
            E.g. ("matchme", 'C') and (start row=2) --> formula= '=$C2="matchme"'
            col can be string (e.g. 'C') or 1-indexed integer (e.g. 3 corresponds with 'C').
        '''
        start, end = self._data_range(start, end, only, as_cell_range_index=True, **kw)
        istart, iend = start.index1, end.index1
        if color is None:
            raise NotImplementedError('add_conditional_formatting_easy(..., color=None)')
        color_rgb = hex_to_rgb_dict(color)
        format_ = {'backgroundColorStyle': {'rgbColor': color_rgb}}
        if font_color is not None:
            font_color_rgb = hex_to_rgb_dict(font_color)
            format_['textFormat'] =  {'foregroundColorStyle': {'rgbColor': font_color_rgb}}
        if formula is not None:
            condition_type = 'CUSTOM_FORMULA'
            condition_values = [formula]
        elif if_text is not None:
            condition_type = 'TEXT_EQ'
            condition_values = [if_text]
        elif if_text_in_col is not None:
            condition_type = 'CUSTOM_FORMULA'
            text_to_match, col = if_text_in_col
            cmatch = CellIndex.implied_from(col, imode=1, imply_col=True)
            formula = f'=${cmatch.colstr}{start.rowstr}="{text_to_match}"'
            condition_values = [formula]
        else:
            raise InputMissingError("must provide 'formula', 'if_text', or 'if_text_in_col'.")
        return self.add_conditional_formatting(istart, iend, condition_type, format_, condition_values)

    # # # BASIC FORMATTING # # #
    CLEAR_FORMATTING = dict(
        numberFormat = dict(type='NUMBER_FORMAT_TYPE_UNSPECIFIED'),
        wrapStrategy = 'CLIP',
        textFormat   = dict(bold=False, italic=False, underline=False),
    )

    @format_docstring(_data_range_docs=_data_range_docs)
    def clear_basic_formatting(self, start=None, end=None, only=None):
        '''clears basic formatting for the specified cells (default: all cells)

        sets dict(numberFormat = dict(type='NUMBER_FORMAT_TYPE_UNSPECIFIED'),
                  wrapStrategy = 'CLIP',
                  textFormat   = dict(bold=False, italic=False, underline=False),
                  )

        {_data_range_docs}
        '''
        crange = self._data_range(start, end, only)
        self.apply_format(crange, self.CLEAR_FORMATTING)

    def row_bold(self, row):
        '''formats text in row to be bold. row should be integer (1-indexed).
        For more complicated formatting try self.apply_format(...) instead.
        '''
        formatting = {'textFormat': {'bold': True}}
        dr = ((row, 1), (row, None))  # from col 1 to right-most col
        self.apply_format(dr, formatting)

    def row_wrap(self, row=1):
        '''formats text in row to use wrapStrategy: 'WRAP'.
        Uses self.CLEARED_FORMATTING but wrapStrategy 'WRAP' instead of 'CLIP'.
        '''
        formatting = {**self.CLEAR_FORMATTING, 'wrapStrategy': 'WRAP'}
        dr = ((row, 1), (row, None))  # from col 1 to right-most col
        self.apply_format(dr, formatting)

    def row_wrap_and_bold(self, row=1):
        '''formats text in row to use wrapStrategy: 'WRAP', and bold text.'''
        formatting = {**self.CLEAR_FORMATTING, 'wrapStrategy': 'WRAP', 'textFormat': {'bold': True}}
        dr = ((row, 1), (row, None))  # from col 1 to right-most col
        self.apply_format(dr, formatting)

    def adjust_column_width_to_fit_header(self, col_index, text, filter=True, *, min_pixels=100, **kw):
        '''tries to adjust column width of column col_index (1-indexed) to fit text.
        If filter, add some width to make room for the arrow for filter.
        [TODO] make this better...
        '''
        width = headersize_pixels(text, filter=filter, **kw)
        px = max(width, min_pixels)
        self.adjust_column_width(col_index, pixel_size=int(px))

    def get_column_widths(self):
        '''returns widths of columns in self.'''
        cellrange = self._data_range('A1', '1', only=None, as_cell_range_index=True).indexstr  # header row only.
        read = self.client.sheet.get(self.spreadsheet.id, ranges=[f'{self.title}!{cellrange}'], includeGridData=True)
        return [int(col_meta['pixelSize']) for col_meta in read['sheets'][0]['data'][0]['columnMetadata']]

    def set_column_widths(self, new_widths):
        '''adjusts widths of columns in self to new_widths.
        new_widths should be a list of widths for all columns starting from 1;
        Use None to skip width adjustment for that column.
        '''
        with self.batching(passthrough=True):
            for i, width in new_widths.items():
                self.adjust_column_width(i+1, pixel_size=width)

    def clear_formats(self, column_width=100):
        '''clears formatting & data validation for WallChart wsheet.
        clears basic formatting, conditional formatting, and data validation.
        sets columns to a standard width.
        '''
        with self.batching(passthrough=True):
            self.adjust_column_width(1, end=self.cols, pixel_size=100)
            self.clear_basic_formatting()
            self.clear_data_validation()
            self.clear_conditional_formatting()


''' --------------------- SheetsDoc --------------------- '''

class SheetsDoc(Spreadsheet):
    '''pygsheets Spreadsheet with custom methods attached;
    also gives SheetsChart instead of Worksheet when accessing a worksheet.
    
    initialize by entering a pygsheets spreadsheet object.
    "spreadsheet" is an entire google sheets file.
    '''
    worksheet_cls = SheetsChart   # "my worksheets are SheetsCharts".

    def __init__(self, spt):
        self.spt = spt
        Spreadsheet.__init__(self, spt.client, spt._jsonsheet, spt._id)

    @classmethod
    @format_docstring(paramdocs=_open_spreadsheet_paramdocs)
    def from_client(cls, client, key=None, *, title=None, url=None, as_json=None):
        '''create SheetsDoc from client and info about the google doc to open.

        {paramdocs}
        '''
        spreadsheet = open_spreadsheet(client, key=key, title=title, url=url, as_json=as_json)
        return cls(spreadsheet)

    @format_docstring(worksheets_doc=Spreadsheet.worksheets.__doc__)
    def sheets_dict(self, *args, sheets=None, exclude_sheets=None, **kw):
        '''return a dict of {{sheetname: SheetsChart for sheet in self.worksheets()}}.

        sheets: None or iterable which allows repeated iteration, e.g. list
            if provided, only use the sheets with title in this list,
            and ensure that all sheets from this list are present in the result.
            i.e. set(result.keys()) == set(sheets) is guaranteed.
            raise PatternError if that is not the case.
        exclude_sheets: None or iterable which allows repeated iteration, e.g. list
            if provided, exclude any sheets with title in this list.

        *args and **kw go to self.worksheets.

        >> Docs from self.worksheets: <<
        {worksheets_doc}
        '''
        # helper function
        def should_use_wks(title):
            if exclude_sheets is not None:
                if title in exclude_sheets:
                    return False
            if sheets is not None:
                return title in sheets
            return True
        # result
        result = {wks.title: wks for wks in self.worksheets(*args, **kw) if should_use_wks(wks.title)}
        # bookkeeping
        if (sheets is not None) and (len(sheets) != len(result)):
            errmsg = "some sheets from 'sheets' kwarg not found. " + \
                     f"sheets={sheets}, found={result.keys()}"
            raise PatternError(errmsg)
        # result
        return result

    def sheets_dict_simple(self):
        '''returns dict of {title: worksheet for worksheet in self.worksheets()}'''
        return {worksheet.title: worksheet for worksheet in self.worksheets()}

    def keys(self):
        '''returns tuple of worksheet titles for sheets in self'''
        return tuple(worksheet.title for worksheet in self.worksheets())

    sheets = alias_to_result_of('keys')

    def values(self):
        '''returns tuple of worksheets for sheets in self'''
        return self.worksheets()

    def items(self):
        '''returns tuple of (title, worksheet) for sheets in self'''
        return tuple((worksheet.title, worksheet) for worksheet in self.worksheets())

    def __getitem__(self, key):
        '''return sheet for this key. key can be int or sheet name (str).'''
        result = super().__getitem__(key)
        if result is not None:
            return result
        # else:
        sheets_dict = self.sheets_dict_simple()
        return sheets_dict[key]

    def __repr__(self):
        return f'{type(self).__name__}(title={repr(self.title)}, sheets={self.keys()})'


''' ---------------------  bookkeeping --------------------- '''

# tell SheetsCharts that their spreadsheet parents would be SheetsDoc objects.
SheetsChart.spreadsheet_cls = SheetsDoc