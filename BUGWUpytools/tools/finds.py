"""
File purpose: find first index/indices of element/s in list, via find/multifind

These are in their own file to mitigate cyclic import errors with iterables.py.
"""

from .equality import (
    equals,
)


''' --------------------- Find --------------------- '''

def find(x, element, default=None, equals=equals):
    '''find smallest index i for which equals(x[i], element);
    return None if no such i exists.
    '''
    try:
        return next(i for i, elem in enumerate(x) if equals(elem, element))
    except StopIteration:
        return default


''' --------------------- Argmin/max --------------------- '''

def argmin(obj):
    '''index of minimum value in obj'''
    return min(range(len(obj)), key=lambda x: obj[x])

def argmax(obj):
    '''index of maximum value in obj'''
    return max(range(len(obj)), key=lambda x: obj[x])