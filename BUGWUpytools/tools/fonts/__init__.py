"""
Saves file paths to font files.
"""

import os

def _font_path_here(font):
    '''returns path to {font}.ttf.'''
    here = __file__
    return os.path.abspath(os.path.join(here, f'../{font}.ttf'))


FONT_PATH_ARIAL = _font_path_here('arial')
FONT_PATH_ARIAL_BOLD = _font_path_here('arial_bold')


FONT_PATH_LOOKUP = {'arial': FONT_PATH_ARIAL,
                    'arial_bold': FONT_PATH_ARIAL_BOLD,
                    }
