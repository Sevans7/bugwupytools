"""
File Purpose: the Chart class
"""

import numpy as np

from ..imports import ImportFailed
try:
    import pandas as pd
except ImportError:
    pd = ImportFailed('pd')


from .charts_tools import (
    CellIndex, CellRangeIndex,
    colstr_to_index, index_to_colstr,
    rowstr_to_index, index_to_rowstr,
    cellstr_to_index, index_to_cellstr,
    rangestr_to_index, index_to_rangestr,
    Indexer, None2,
)
from ..errors import DimensionError, PatternError
from ..pandas_tools import (
    df_write, df_read, df_read_from_excel,
)
from ..oop_misc import alias


class Chart(np.ndarray):
    '''2D numpy array with convenient methods for interfacing to gsheets & excel.
    meta lets the user track any desired meta info.
    _from: if provided, and meta is None, get meta from _from.
    dtype of self for new Charts will default to object,
        to mitigate unexpected dtype issues when combining data.
        (e.g. if string type, setting a cell to a longer string than the string type will cutoff the data.)
    '''
    # # # NUMPY SUBCLASSING # # #
    def __new__(cls, input_array, *, meta=None, _from=None):
        arr = np.asanyarray(input_array)
        if meta is None: meta = getattr(arr, 'meta', None)
        if meta is None: meta = getattr(_from, 'meta', dict())
        obj = cls._new_arr_adjust(arr, meta=meta)
        obj.meta = meta
        return obj

    @classmethod
    def _new_arr_adjust(cls, arr, *, meta=dict()):
        '''adjust numpy array (or subclass) instance arr when creating new instance of cls.
        arr will always be a numpy array. result should be an instance of cls.

        Adjustments here: arr.astype(object). To avoid unexpected type conflicts.
        '''
        arr = arr.astype(object)
        return arr.view(cls)

    def __array_finalize__(self, obj):
        if obj is None: return
        self._init_from(obj)

    def _init_from(self, obj):
        '''called during __array_finalize__.'''
        if self.ndim != 2:
            if self.ndim > 2:
                raise DimensionError(f'Expected ndim <= 2 but got ndim = {ndim}')
            else:
                self.shape = (1, -1)
        self.meta = getattr(obj, 'meta', {})

    def __array_ufunc__(self, ufunc, method, *inputs, out=None, **kwargs):
        '''inputs --> ndarray. outputs --> type(self). Adapted from guide in numpy docs.
        Combine meta from all Chart inputs, keeping leftmost-input values in case of conflict.
        '''
        TYPE = Chart   # <-- Chart type
        def downcast(arr):
            return arr.view(np.ndarray) if isinstance(arr, TYPE) else arr

        # convert inputs to ndarray; prep outputs
        inputs = tuple(downcast(i) for i in inputs)
        outputs = out
        if outputs:
            out_args = tuple(downcast(o) for o in outputs)
            kwargs['out'] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        # perform the ufunc
        results = super().__array_ufunc__(ufunc, method, *inputs, **kwargs)

        # provide result, casting type to TYPE if appropriate.
        if results is NotImplemented:
            return NotImplemented
        if method == 'at':  # performed in-place operation; return None
            return
        if ufunc.nout == 1:  # only 1 output --> do bookkeeping in order to apply code below.
            results = (results,)

        results = tuple((np.asarray(result).view(TYPE)
                         if output is None else output)
                        for result, output in zip(results, outputs))

        if results and isinstance(results[0], TYPE):
            result = results[0]
            result.meta = self._meta_like(*inputs)
            return result

        return results[0] if len(results) == 1 else results

    @classmethod
    def _meta_like(cls, *input_arrays):
        '''returns meta combined from all inputs with same type (or subclass) as cls,
        keeping leftmost-input values in case of conflict.
        '''
        inputs_of_type = tuple(arr for arr in input_arrays if isinstance(arr, cls))
        meta_out = {}
        for arr in inputs_of_type[::-1]:
            meta_out.update(arr.meta)
        return meta_out

    _HANDLED_ARRAY_FUNCTIONS = {}

    def __array_function__(self, func, types, args, kwargs):
        '''does specially-defined array function if possible, else convert arrays to np.ndarray first.'''
        if func not in self._HANDLED_ARRAY_FUNCTIONS:
            if args[0] is self:
                return func(np.asarray(self), *args[1:], **kwargs)
            else:
                return NotImplemented
        # allow subclasses that don't override __array_function__ to handle Chart objects:
        if not all(issubclass(t, Chart) or (t is np.ndarray) for t in types):
            return NotImplemented
        # return result of calling that function
        return self._HANDLED_ARRAY_FUNCTIONS[func](*args, **kwargs)

    @classmethod
    def _implements_array_function(cls, numpy_function):
        '''Register an __array_function__ implementation for Chart objects.'''
        def bookkeeping_f_as_handled_array_function(f):
            cls._HANDLED_ARRAY_FUNCTIONS[numpy_function] = f
            return f
        return bookkeeping_f_as_handled_array_function

    # # # MISC. INSTANTIATION / CREATION # # #
    FILL = ''  # << fill value for new empty cells

    @classmethod
    def empty(cls, shape, fill=None2, **kw__init):
        '''returns empty Chart, filled with fill value.
        Use cls.FILL ('' by default) if fill not entered.
        '''
        try:
            L = len(shape)
        except TypeError:
            L = 1
        if L != 2:
            raise DimensionError(f'Invalid shape! Chart ndim should be 2. Got shape=={shape}')
        if fill is None2: fill = cls.FILL
        arr = cls(np.empty(shape), **kw__init)
        arr[:] = fill
        return arr

    def _new(self, arr):
        '''returns a new instance of type(self) with data in arr but meta from self.'''
        return type(self)(arr, _from=self)

    @classmethod
    def from_header_and_data(cls, header, data, **kw__init):
        '''return Chart with header as row 0 and data as remaining rows.'''
        data_orig = data
        data = np.asanyarray(data)
        nrow = 1 + data.shape[0]
        ncol = len(np.squeeze(header))
        result = cls.empty((nrow, ncol), **kw__init)
        result[0] = header
        result[1:] = data
        return result

    @classmethod
    def from_df(cls, df, *, row_0_as_header=False, **kw__init):
        '''return Chart with info from DataFrame df.
        row_0_as_header: bool, default False
            whether df came from using row 0 as header.
            If True, put header into row 0 before returning result.
            i.e. result[0, :] == header of df.
        '''
        arr = df.to_numpy()
        if row_0_as_header:
            header_as_chart_row = df.columns.to_numpy().reshape(1,-1)
            arr = np.concatenate((header_as_chart_row, arr), axis=0)
        return cls(arr, **kw__init)

    @classmethod
    def from_dict(cls, dict_, **kw__init):
        '''return Chart with data from dict_.
        keys become value at row 0; values become columns (after row 0).
        '''
        ncol = len(dict_)
        nrow = max(len(val) for val in dict_.values()) + 1  # +1 accounts for the header row.
        result = cls.empty((nrow, ncol), **kw__init)
        for icol, (key, col) in enumerate(dict_.items()):
            result[0 , icol] = key
            result[1:, icol] = col
        return result

    # # # CONVENIENT SIMPLE PROPERITES # # #
    nrow = property(lambda self: self.shape[0], '''number of rows in self.''')
    ncol = property(lambda self: self.shape[1], '''number of columns in self.''')

    # # # CONVENIENT INDEXING PROPERTIES # # #
    header = property(lambda self: self.get_row(0),
                      doc='''column labels, i.e. top row (index 0) as an array.''')
    data = property(lambda self: self.values[1:],
                    doc='''data, i.e. rows 1+, as an array.''')

    # # # INDEXING # # #
    CellIndex = CellIndex  # << accessible here for convenience only. Can help with indexing.
    CellRangeIndex = CellRangeIndex  # << accessible here for convenience only. Can help with indexing.

    def cell(self, s):
        '''returns value of cell at string s (excel convention, e.g. 'A1' --> arr[0,0]).'''
        return self[cellstr_to_index(s)]

    def cells(self, s):
        '''returns values of cells at string s (excel convention, e.g. 'A5:C9' --> arr[4:9, 0:3]).'''
        return self[rangestr_to_index(s)]

    def cell_index(self, s):
        '''returns values of cell(s) at string s.
        if s represents a range, result will be a Chart; otherwise result will be a single value.
        '''
        return self.cells(s) if (':' in s) else self.cell(s)

    def __getitem__(self, index):
        '''uses usual indexing rules, but if index is a string use self.cell_index(index) instead.'''
        if isinstance(index, str):
            return self.cell_index(index)
        else:
            return super().__getitem__(index)

    def plain_index(self, i):
        '''returns np.ndarray form of value(s) of cell(s) at indicated index.
        Equivalent to np.asarray(self)[i]
        '''
        return np.asarray(self)[i]

    def get_row(self, i):
        '''returns i'th row, as np.ndarray.
        (Note: just using self[i] instead returns a Chart with shape (1, self.ncol).)
        '''
        # return self.iloc[i, :]  # << works fine, slightly less efficient.
        return np.asarray(self[i, :])[0, :]  # << slightly more efficient.

    def get_col(self, i):
        '''returns i'th column, as np.ndarray.
        (Note: just using self[:, i] instead returns a Chart with shape (1, self.nrow).)
        '''
        # return self.iloc[:, i]  # << works fine, slightly less efficient.
        return np.asarray(self[:, i])[0, :]  # << slightly more efficient.

    def row_dicts(self):
        '''iterator of rows, returning dict of {header label: value} for each row.
        skips row 0 (assuming that's the header).
        '''
        header = self.header
        for row in self.values[1:]:
            yield dict(zip(header, row))

    @property
    def values(self):
        '''returns np.asarray(self)'''
        return np.asarray(self)

    @property
    def idat(self):
        '''indexer for the data in self. Indexing result will index data but keep header.
        E.g. self.idat[4:, [5,7]] will be a Chart like self but skipping data rows 0 through 3,
            and taking only columns 5 & 7.
        '''
        def _finalize(arr, index):
            header = self.header
            if isinstance(index, tuple) and len(index)==2:
                header = header[index[1]]
            return self.from_header_and_data(header, arr, _from=self)
        return Indexer(self.data, _finalize)

    # # # INSPECT # # #
    def column_index_from_label(self, label):
        '''returns first i such that self[0, i] == label. if not found, raise IndexError.'''
        where = self[0, :].where_1d(label)
        if len(where) == 0:
            raise IndexError(f'label not found in row 0: {repr(label)}')
        else:
            return where[0]

    def column_indices_from_labels(self, *labels):
        '''returns tuple(self.column_index_from_label(label) for label in labels)'''
        return tuple(self.column_index_from_label(label) for label in labels)

    icol = icolumn = alias('column_index_from_label')
    icols = icolumns = alias('column_indices_from_labels')

    def column(self, label, *, keep_header=False):
        '''returns data from column with this label.
        Equivalent to self.get_col(self.column_index_from_label(label))[1:].

        If keep_header, instead keep row 0 in the result.
        '''
        index = self.icolumn(label)
        result = self.get_col(index)
        return result if keep_header else result[1:]

    def columns(self, *labels, keep_header=False):
        '''returns array of self indexed by columns at these labels, for row [1:].'''
        indices = self.icolumns(*labels)
        return np.asarray(self[:, indices]) if keep_header else np.asarray(self[1:, indices])

    col = alias('column')
    cols = alias('columns')

    def column_dict(self, labels):
        '''returns dict with keys column labels, values column data.'''
        result = dict()
        for label in labels:
            result[label] = self.column(label)
        return result

    def has_column(self, label):
        '''returns whether self has column labeled with this label.'''
        return label in self[0, :]

    def has_value_in_column(self, column_label, value):
        '''returns whether self has column with column_label, containing value.
        (If same-named column appears multiple times, only consider the first appearance.)
        '''
        try:
            i_column = self.column_index_from_label(column_label)
        except IndexError:  # self doesn't even have this column...
            return False
        return value in self[:, i_column]

    # # # VALUE MATCHING # # #
    def whereTrue(self):
        '''return list of all (irow, icol) pairs for which self[irow, icol] is True.
        Equivalent to list(zip(*np.where(self))).
        '''
        return list(zip(*np.where(self)))

    def where(self, value):
        '''return list of all (irow, icol) pairs for which self[irow, icol] == value.'''
        return (self == value).whereTrue()

    def whereTrue_1d(self):
        '''return list of all i for which np.squeeze(self)[i] is True.
        (Provided for convenience since regular indexing of self always gives a 2d array.)
        If np.squeeze(self).ndim > 1, raise DimensionError.
        '''
        arr = np.squeeze(self)
        if arr.ndim > 1:
            raise DimensionError(f'where1d expects 1d array after squeezing, but got ndim={arr.ndim}')
        return np.where(arr)[0]

    def where_1d(self, value):
        '''return list of all i for which np.squeeze(self)[i] == value.
        (Provided for convenience since regular indexing of self always gives a 2d array.)
        If np.squeeze(self).ndim > 1, raise DimensionError.
        '''
        return (self == value).whereTrue_1d()

    def where_row(self, row):
        '''return list of all i for which self[i] is an exact match for row.'''
        allmatch = np.all(self == row, axis=1)
        return np.where(allmatch)[0]

    def where_value_in_column(self, column_label, value, keep_header=False):
        '''returns list (actually, array) of all i for which self.col(column_label)==value.
        keep_header: bool, default False
            whether to keep header row.
            if True, then self[i_from_result, i_column] == value.
            if False, then self[1 + i_from_result, i_column] == value.
        '''
        col = self.col(column_label, keep_header=keep_header)
        where = np.where(col == value)  # list of lists of indices. One list of indices per dim. Only 1 dim.
        return where[0]

    def lookup(self, value, col_in_label, col_out_label, *, list_if_single=True):
        '''returns list (actually, array) of self[i_row, i_col_out], where self[i_row, i_col_in]==value.
        col_in and col_out should be labels for columns; we will do self.col(col_in) and self.col(col_out).
        list_if_single: bool, default True
            whether to keep result as a list if length is 1.
            if False and len(result)==1, return result[0] instead.
        '''
        where = self.where_value_in_column(col_in_label, value)
        arr_out = self.col(col_out_label)
        result = arr_out[where]
        return result if list_if_single or len(result)!=1 else result[0]

    def lookup_dict(self, col_in_label, col_out_label, *,
                    force_unique=False, list_if_single=False,
                    keep_header=False, ignore=''):
        '''returns dict of {self at (irow, i_col_in) : self at (irow, i_col_out)}.
        if the value self[irow, i_col_in] appears in multiple rows,
            the value in the dict will be a list. E.g. {'duplicate_key': ['value0', 'value1', 'value2']}

        force_unique: bool, default False
            whether to force all values in col_in to be unique.
            if True, crash with PatternError if any duplicates are found in col_in.
        list_if_single: bool, default False
            if True, the values in the dict will always be a list.
            True  --> {'unique_key': ['value'], 'dup_key': ['v0', 'v1', 'v2']}
            False --> {'unique_key': 'value', 'dup_key': ['v0', 'v1', 'v2']}
        keep_header: bool, default False
            whether to keep header when getting columns.
            if True, (col_in_label --> col_out_label) will appear in the result.
        ignore: string or list of strings, default ''
            ignore these col_in values. I.e., if value in ignore, skip value.
            (by default, only skip empty string.)
        '''
        arr_in = self.col(col_in_label, keep_header=keep_header)
        arr_out = self.col(col_out_label, keep_header=keep_header)
        result = dict()
        for irow, (val_in, val_out) in enumerate(zip(arr_in, arr_out)):
            if val_in in ignore:
                continue
            try:
                result[val_in].append(val_out)
            except KeyError:
                result[val_in] = [val_out]
            else:
                if force_unique:
                    errmsg = f'force_unique=True, but got duplicate ({repr(val_in)}) in input column ({repr(col_in_label)})'
                    raise PatternError(errmsg)
        if not list_if_single:
            for key, val in result.items():
                if len(val) == 1:
                    result[key] = val[0]
        return result

    def ndiffs(self, chart):
        '''return (number of different rows, number of different cells),
        after possibly adding empty rows and/or columns to self and/or chart so their shapes match.
        '''
        cself, cchart = self.match_shapes(chart)
        diffs = (cself != cchart)
        ndiff_cells = np.count_nonzero(diffs)
        ndiff_rows = np.count_nonzero(np.any(diffs, axis=1))
        return (ndiff_rows, ndiff_cells)

    # # # DF METHODS # # #
    def drop_duplicates(self, subset=None, keep='first', **kw__drop_dups):
        '''returns Chart with duplicates removed.
        subset, keep, and other inputs are passed to DataFrame.drop_duplicates().
        See help(self.to_df().drop_duplicates) for accurate signature.
        '''
        df = self.to_df(row_0_as_header=True)
        no_dups = df.drop_duplicates(subset=subset, keep=keep, **kw__drop_dups)
        meta = self._meta_like(self)
        result = self.from_df(no_dups, row_0_as_header=True, meta=meta)
        return result
        
    # # # RESHAPING / ADD ROWS OR COLUMNS# # #
    def append_empty_rows(self, n, fill=None2):
        '''returns result of appending n empty rows (filled with fill value provided).
        If fill is not provided, use self.FILL (default '').
        if n == 0, return self, unchanged.
        '''
        if n==0: return self
        if fill is None2: fill = self.FILL
        empty_rows = self.empty((n, self.ncol), fill=fill)
        return self.append_rows(empty_rows)

    def append_empty_cols(self, n, fill=None2):
        '''returns result of appending n empty columns (filled with fill value provided).
        If fill is not provided, use self.FILL (default '').
        if n == 0, return self, unchanged.
        '''
        if n==0: return self
        if fill is None2: fill = self.FILL
        empty_cols = self.empty((self.nrow, n), fill=fill)
        return self.append_cols(empty_cols)

    def expand_shape(self, new_nrow, new_ncol):
        '''returns result with shape (new_nrow, new_ncol) after appending empty rows/cols to self.
        raise DimensionError if self.nrow > new_nrow or self.ncol > new_ncol.
        '''
        nrow, ncol = self.shape
        if nrow > new_nrow:
            raise DimensionError(f'new nrow ({new_nrow}) < self.nrow ({nrow})!')
        if ncol > new_ncol:
            raise DimensionError(f'new ncol ({new_ncol}) < self.ncol ({ncol})!')
        result = self.append_empty_rows(new_nrow - nrow)
        result = result.append_empty_cols(new_ncol - ncol)
        return result

    def matching_shape(self, other_chart):
        '''returns minimum shape that fits self and other chart.'''
        return (max(self.nrow, other_chart.nrow), max(self.ncol, other_chart.ncol))

    def match_shapes(self, other_chart):
        '''returns (self, other_chart) but expanded to the minimum shape that fits both of them.'''
        nrow, ncol = self.matching_shape(other_chart)
        return (self.expand_shape(nrow, ncol), other_chart.expand_shape(nrow, ncol))

    def minimum_shape_fitting(self, x):
        '''returns minimum shape fitting self and x. (row, col).'''
        xshape = x.shape
        assert len(xshape) == 2, f"expected x with 2D shape but got len(x.shape)=={len(xshape)}"
        xrow, xcol = xshape
        return (max(self.nrow, xrow), max(self.ncol, xcol))

    def expand_shape_to_fit(self, x):
        '''returns self, expanded to minimum shape fitting self and x.'''
        shape = self.minimum_shape_fitting(x)
        return self.expand_shape(*shape)

    comparables = alias('match_shapes')

    def append_rows(self, rows):
        '''returns result of appending rows to self.
        Equivalent to np.concatenate((self, rows), axis=0).
        '''
        return np.concatenate((self, rows), axis=0)

    def append_cols(self, cols_transposed):
        '''returns result of appending cols to self.
        Equivalent to np.concatenate((self, cols_transposed), axis=1).
        Note: cols_transposed[i][j] represents row i, col j.
            I.e. it is a list of rows, not a list of cols.
            (that is why "_transposed" appears in the variable name.)
        '''
        return np.concatenate((self, cols_transposed), axis=1)

    def append_col_list(self, col_list):
        '''returns result of appending this list of columns to self.
        Equivalent to np.concatenate((self, np.array(col_list).T), axis=1).
        Note: col_list[i][j] represents col i, row j.
            I.e. it is a list of cols, not a list of rows.
        '''
        col_list_T = np.array(col_list).T
        return np.concatenate((self, col_list_T), axis=1)

    def append_col_dict(self, col_dict):
        '''returns result of appending this dict of columns to self.
        Uses keys as row 0, values as the remaining rows, for each column.
        '''
        new_cols = self.from_dict(col_dict)
        return self.append_cols(new_cols)

    def append_col_if_missing(self, label):
        '''returns result of making & appending a col if self doesnt have column with this label.'''
        if self.has_column(label):
            return self
        # else, need to append col with this label.
        col = self.empty((self.nrow, 1))
        col[0,0] = label
        return self.append_cols(col)

    # # # REMOVE ROWS # # #
    def remove_rows(self, idx, *, includes_header=False):
        '''remove the rows with these indices from self; returns result.

        idx: list
            indices of rows to remove. 0-indexed, i.e. 0 is the first row.
        includes_header: bool, default False
            True --> input idx=1 for the first row with data. idx=0 refers to the header.
            False --> input idx=0 for the first row with data.
        '''
        if not includes_header:
            idx = [i+1 for i in idx]
        return np.delete(self, idx, axis=0)

    # # # RELABELING COLUMNS # # #
    def relabel_col(self, old_label, new_label, *, only_first=True, missing_ok=False, copy=True):
        '''replace the first occurence of old_label in row 0 with new_label.

        only_first: bool, default True
            whether to replace only the first occurrence, if multiple are found.
            False --> replace all occurrences.
        missing_ok: bool, default False
            whether it's okay if old_label is missing in row 0.
            False --> make KeyError if old_label is missing.
        copy: bool, default True
            whether to copy chart before relabeling.
            True --> operation is performed on copy
            False --> operation is performed in-place, i.e. self is changed directly.

        return relabeled chart.
        '''
        iold = list(self[0].where_1d(old_label))
        if len(iold) == 0:
            if missing_ok:
                return []
            else:
                raise KeyError(f'{repr(old_label)} not found in row 0, and missing_ok=False')
        if only_first:
            iold = iold[:1]
        if copy:
            self = self.copy()
        for i in iold:
            self[0, i] = new_label
        return self

    # # # DATA FRAMES # # #
    def _row_labels(self):
        '''return row labels for self, in excel notation, as strings.'''
        return [index_to_rowstr(i) for i in range(self.nrow)]

    def _col_labels(self):
        '''return col labels for self, in excel notation, as strings.'''
        return [index_to_colstr(i) for i in range(self.ncol)]

    def to_df(self, *, row_0_as_header=False, **kw):
        '''returns pd.DataFrame(self, **kw). Use excel notation for row & column labels.
        row_0_as_header: bool, default False
            if True, and 'columns' not in kw, use row 0 as the header row
                (instead of excel notation) and do not include row 0 in the data.
        '''
        if 'columns' not in kw:
            if row_0_as_header:
                header = self.header
                self = self[1:]
            else:
                header = self._col_labels()
            kw['columns'] = header
        if 'index' not in kw:  # << note, must occur after potential moving row 0 into header!
            kw['index'] = self._row_labels()
        return pd.DataFrame(self, **kw)

    # # # DISPLAY # # #
    def _ipython_display_(self):
        '''This is the method jupyter notebook uses to decide how to display self.'''
        import IPython.display as ipd
        ipd.display(self.to_df())

    def __repr__(self):
        contents = ', '.join(self._repr_contents())
        return f'{type(self).__name__}({contents})'

    def _repr_contents(self):
        '''return list of contents to go into repr of self.'''
        return [f'nrow={self.nrow}', f'ncol={self.ncol}']

    def __str__(self):
        return f'{self.__repr__()}:\n{super().__str__()}'

    # # # READ / WRITE # # #
    def df_write(self, filename, *args, mode='excel', dryrun=False, **kw):
        '''write self to filename by converting to pandas DataFrame then using df_write.'''
        df = self.to_df()
        return df_write(df, filename, *args, mode=mode, dryrun=dryrun, **kw)

    def to_excel(self, filename, sheet_name='UntitledSheet', dryrun=False, **kw__to_excel):
        '''alias for self.df_write(..., mode='excel').
        filename should end in .xlsx, else .xlsx will be appended to it.

        dryrun: bool, default False
            if dryrun, don't actually write anything, but still return the same thing.

        Writes self to sheet_name in excel file at filename.
        '''
        return self.df_write(filename, sheet_name=sheet_name, dryrun=dryrun, **kw__to_excel)

    @classmethod
    def df_read(cls, filename, *args, sheet_name=None, mode='excel', meta=dict(), kw__init=dict(), **kw):
        '''read file via df_read then convert from DataFrame to type==cls (probably a Chart).
        returns a Chart, OR a dict of Charts (if sheet_name is None and mode is 'excel').

        meta: dict
            use as result.meta if result is a single Chart
            if result is a dict, set val.meta = meta.copy() for each val in result.values.
        '''
        df_or_dict = df_read(filename, *args, sheet_name=sheet_name, mode=mode, **kw)
        if isinstance(df_or_dict, dict):
            dict_ = df_or_dict
            return {key: cls(val.to_numpy(), meta=meta.copy(), **kw__init) for key, val in dict_.items()}
        else:
            df = df_or_dict
            return cls(df, meta=meta, **kw__init)

    @classmethod
    def from_excel(cls, filename, sheet_name=None, *, custom_ext=False,
                   read_only=True, meta=dict(), kw__init=dict(), **kw__load_workbook):
        '''alias to self.df_read(..., mode='excel').
        read sheet_name in excel file at filename, returning Chart
        or a dict of Charts with keys sheet names, if sheet_name is None.

        filename should end in .xlsx, else .xlsx will be appended to it.
        sheet_name: None or string
            string --> returns DataFrame for just this sheet.
            None --> returns dict of {name: DataFrame for sheet name}
        custom_ext: bool, default False
            if True, actually do not mess with ending of filename.
        meta: dict
            passed as kwarg; cls.__new__(..., meta=meta)
        kw__init: dict
            passed as kwargs; cls.__new__(..., **kw__init)

        additional kwargs are passed to openpyxl.load_workbook.
        '''
        return cls.df_read(filename, sheet_name=sheet_name, mode='excel',
                custom_ext=custom_ext, kw__init=kw__init, meta=meta,
                read_only=read_only, **kw__load_workbook)


# # # ARRAY FUNCTIONS # # #
@Chart._implements_array_function(np.concatenate)
def concatenate(arrays, axis=0, **kw):
    '''concatenates arrays, returning a Chart.
    Actually, returns a subclass of Chart, if any arrays are a subclass of chart.
        returns the "max" subclass of Chart; via isinstance;
        e.g. if Chart2 subclasses Chart1 which subclasses Chart,
            result will be Chart2 if inputs are typed Chart, Chart2, Chart1, array.
            in case of multiple different subclasses,
                (e.g. ChartB subclassing Chart1 but not Chart2)
                depends on which appears first in the order,
                e.g. if inputs are Chart, ChartB, Chart1, Chart2, result will have type ChartB,
                but if inputs are Chart, Chart2, Chart1, ChartB, result will have type Chart2.
        '''
    ndarrs = tuple(array.view(np.ndarray) for array in arrays)
    concat = np.concatenate(ndarrs, axis=axis, **kw)
    maxtype = Chart
    for arr in arrays:
        if isinstance(arr, maxtype):
            maxtype = type(arr)
    result = concat.view(maxtype)
    result.meta = maxtype._meta_like(*arrays)
    return result

@Chart._implements_array_function(np.char.strip)
def char_strip(array, chars=None):
    '''element-wise strip chars (default: whitespace) from array, returning a Chart.
    (or a subclass of Chart, if array is a subclass of Chart.)
    '''
    arr_cls = type(array)
    ndarr = array.view(np.ndarray).astype(str)
    stripped = np.char.strip(ndarr, chars=chars)
    result = stripped.view(arr_cls).astype(array.dtype)
    result.meta = result._meta_like(array)
    return result

@Chart._implements_array_function(np.delete)
def delete(array, idx, axis=0):
    '''delete idx in array, along axis, returning a Chart.
    idx=0 refers to header row (if axis=0).
    '''
    arr_cls = type(array)
    ndarr = array.view(np.ndarray)
    deleted = np.delete(ndarr, idx, axis=axis)
    result = deleted.view(arr_cls)
    result.meta = result._meta_like(array)
    return result
