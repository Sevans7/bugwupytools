"""
File Purpose: misc. tools for Chart objects
"""

from ..arrays import list_shape

''' --------------------- Basic Indexing --------------------- '''
# and converting index numbers to string and vice versa.

def _Alpha2num(A):
    '''converts A (a string of length 1) to numeric value in the alphabet.
    "A" --> 0; "B" --> 1; ... "Z" --> 25.
    '''
    result = ord(A) - ord('A')
    assert 0 <= result <= 25, f"expected capital letter but got: '{A}'"
    return result

def _num2Alpha(n):
    '''converts n (an integer with 0 <= n <= 25) to string in the alphabet.
    0 --> "A"; 1 --> "B"; ... 25 --> "Z".
    '''
    assert 0 <= n <= 25, f"expected 0 <= n <= 25 but got n={n}"
    return chr(n + ord('A'))

BASE = 26

def colstr_to_index(s):
    '''converts column string to column index. (0-indexed)
    See also: https://stackoverflow.com/a/45312360
    "A" --> 0
    "B" --> 1
    ...
    "Z" --> 25
    "AA" --> 26
    "AB" --> 27
    ...
    "BA" --> 26*2
    ...
    "AAA" --> 26**2
    "AAB" --> 26**2 + 1
    ...
    "ABA" --> 26**2 + 26*2
    ...
    '''
    n = 0
    for c in s:
        n = n * BASE + 1 + _Alpha2num(c)
    return n - 1

def index_to_colstr(i):
    '''converts column index to column string. (0-indexed)
    See also: https://stackoverflow.com/a/45312360
    '''
    n = i + 1
    name = ''
    while n > 0:
        n, r = divmod (n - 1, BASE)
        name = chr(r + ord('A')) + name
    return name

def rowstr_to_index(s):
    '''converts row string to row index. == int(s) - 1. (0-indexed)'''
    return int(s) - 1

def index_to_rowstr(i):
    '''converts row index to row string. == str(i + 1). (0-indexed)'''
    return str(i + 1)

def cell_str_split(s):
    '''splits cell string s into (column string, row string).
    E.g. 'A1' --> ('A', '1');  'ABZ27' --> ('ABZ', '27').
    '''
    i = 0
    while i<len(s) and (0 <= ord(s[i]) - ord('A') <= 25):
        i += 1
    return (s[:i], s[i:])

def cellstr_to_index(s):
    '''returns index of cell given string; (row index, column index). (0-indexed)'''
    colstr, rowstr = cell_str_split(s)
    return (rowstr_to_index(rowstr), colstr_to_index(colstr))

def index_to_cellstr(i):
    '''returns cell string given index (row index, column index). (0-indexed)'''
    rowi, coli = i
    return index_to_colstr(coli) + index_to_rowstr(rowi)


''' --------------------- Indexing One Cell --------------------- '''

class CellIndex():
    '''helps manage indexing for a single cell value.
    see help(type(self)) for details on the properties in self.
    '''
    # # # INITIALIZATION / CREATION # # #
    def __init__(self, row=None, col=None, *, index=None,
                 rowstr=None, colstr=None, indexstr=None,
                 row1=None, col1=None, index1=None):
        kw = dict(row=row, col=col, index=index,
                  rowstr=rowstr, colstr=colstr, indexstr=indexstr,
                  row1=row1, col1=col1, index1=index1)
        for key, val in kw.items():
            if val is not None: setattr(self, key, val)

    @classmethod
    def implied_from(cls, arg, *, imode=0, imply_col=False):
        '''intelligently create CellIndex based on arg. Behavior depends on what arg looks like.
        arg possibilities:
            int --> row = arg.  (0-indexed)  (unless imply_col; in which case, col = arg.)
            tuple --> (row, col) = arg.  (0-indexed)
            str --> get rowstr, colstr, or indexstr info, depending on string contents,
                    e.g. '37' --> row=36;  'B' --> col=1;  'B37' --> row, col = (36, 1).
            already of type cls --> return arg, unchanged.
        imode: 0 or 1, default 0
            indexing mode. 0 <--> inputs are 0-indexed. 1 <--> inputs are 1-indexed.
            (Only matters if inputs are int / tuple of int.)
        imply_col: bool, default False
            whether an int arg implied col index (instead of row).
        '''
        assert imode in (0, 1), f'invalid imode, expected 0 or 1 but got {imode}'
        if isinstance(arg, tuple):
            return cls(index=arg) if imode==0 else cls(index1=arg)
        elif isinstance(arg, int):
            if imply_col:
                return cls(col=arg) if imode==0 else cls(col1=arg)
            else:
                return cls(row=arg) if imode==0 else cls(row1=arg)
        elif isinstance(arg, str):
            colstr, rowstr = cell_str_split(arg)
            kw_init = dict()
            if len(rowstr) > 0:
                kw_init['rowstr'] = rowstr
            if len(colstr) > 0:
                kw_init['colstr'] = colstr
            return cls(**kw_init)
        elif isinstance(arg, cls):
            return arg
        else:
            raise TypeError(f'Expected int, tuple, str, or {cls.__name__}, but got arg of type {type(arg).__name__}')

    # # # BASIC PROPERTIES # # #
    def _set_index(self, new_row_and_col):
        self.row, self.col = new_row_and_col

    def __iter__(self):
        yield self.row
        yield self.col

    row = property(lambda self: getattr(self, '_row', None), lambda self, val: setattr(self, '_row', val),
                    doc='''0-indexed row. None if not known.''')
    col = property(lambda self: getattr(self, '_col', None), lambda self, val: setattr(self, '_col', val),
                    doc='''0-indexed col. None if not known.''')
    index = property(lambda self: (self.row, self.col), lambda self, val: self._set_index(val),
                    doc='''0-indexed (row, col) tuple. either or both can be None if not known.''')

    # # # MORE ADVANCED PROPERTIES # # #
    rowstr = property(lambda self: index_to_rowstr(self.row),
                      lambda self, value: setattr(self, 'row', rowstr_to_index(value)),
                      doc='''cell's row string. E.g. 'A' for row 0.''')
    colstr = property(lambda self: index_to_colstr(self.col),
                      lambda self, value: setattr(self, 'col', colstr_to_index(value)),
                      doc='''cell's col string. E.g. '1' for col 0.''')
    indexstr = property(lambda self: index_to_cellstr(self.index),
                        lambda self, value: setattr(self, 'index', cellstr_to_index(value)),
                        doc='''cell's index string. E.g. 'A7' for row 0, col 6.''')

    def _set_index1(self, new_row1_and_col1):
        self.row1, self.col1 = new_row1_and_col1

    row1 = property(lambda self: None if self.row is None else self.row + 1,
                    lambda self, value: setattr(self, 'row', None if value is None else value - 1),
                    doc='''1-indexed row. None if not known.''')
    col1 = property(lambda self: None if self.col is None else self.col + 1,
                    lambda self, value: setattr(self, 'col', None if value is None else value - 1),
                    doc='''1-indexed col. None if not known.''')
    index1 = property(lambda self: (self.row1, self.col1), lambda self, val: self._set_index1(val),
                    doc='''1-indexed (row, col) tuple. either or both can be None if not known.''')

    # # # MATH # # #
    def __add__(self, val):
        '''self + val'''
        assert len(val) == 2
        return type(self)(self.row + val[0], self.col + val[1])

    def __radd__(self, val):
        '''val + self'''
        return self.__add__(val)
    
    def __sub__(self, val):
        '''self - val'''
        assert len(val) == 2
        row, col = self.row - val[0], self.col - val[1]
        assert (row > 0) and (col > 0)
        return type(self)(row, col)

    def __rsub__(self, val):
        '''val - self'''
        assert len(val) == 2
        row, col = val[0] - self.row, val[1] - self.col
        assert (row > 0) and (col > 0)
        return type(self)(row, col)

    # # # DISPLAY # # #
    def __repr__(self):
        return f'{type(self).__name__}(row={self.row}, col={self.col})'

    def __str__(self):
        return repr(self.indexstr)


''' --------------------- Indexing Multiple Cells --------------------- '''

class CellRangeIndex():
    '''helps manage indexing for two cell values (top left & bottom right corners of a range).
    see help(type(self)) for details on the properties in self.
    '''
    ci_cls = CellIndex   # << when my methods create CellIndex objects, they will use this class.

    # # # INITIALIZATION / CREATION # # #
    def __init__(self, start_ci=None, end_ci=None, *, index_ci=None,
                 start=None, end=None, index=None,
                 startstr=None, endstr=None, indexstr=None,
                 start1=None, end1=None, index1=None):
        kw = dict(start_ci=start_ci, end_ci=end_ci, index_ci=index_ci,
                  start=start, end=end, index=index,
                  startstr=startstr, endstr=endstr, indexstr=indexstr,
                  start1=start1, end1=end1, index1=index1)
        for key, val in kw.items():
            if val is not None: setattr(self, key, val)

    @classmethod
    def implied_from(cls, arg, array=None, *, imode=0):
        '''intelligently create RangeIndex based on arg. Behavior depends on what arg looks like.
        arg possibilities:
            tuple --> use arg[0] to get start; arg[1] to get end if provided.
                      if arg[0] is not iterable, instead use arg to get start, and use end=None.
            str --> start indexstr or full indexstr, depending on string contents,
                    e.g. 'A1:C7' --> full indexstr;  'A1' --> start indexstr.
            already of type cls --> return arg, unchanged.
        array: None or array (or list of lists)
            if provided, and arg implies only the start index, use array to determine the end index.
            See also: cls.from_start_and_array(arg, array)
        imode: 0 or 1, default 0
            indexing mode. 0 <--> inputs are 0-indexed. 1 <--> inputs are 1-indexed.
            (Only matters if inputs are int / tuple of int.)
        '''
        if isinstance(arg, tuple):
            kw_init = dict()
            if len(arg) >= 1:
                try:
                    iter(arg[0])
                except TypeError:  # arg is like (1,1). Use it to get start.
                    arg = (arg,)
            if len(arg) >= 1:
                kw_init['start_ci'] = cls.ci_cls.implied_from(arg[0], imode=imode)
            if len(arg) >= 2:
                kw_init['end_ci'] = cls.ci_cls.implied_from(arg[1], imode=imode)
            result = cls(**kw_init)
        elif isinstance(arg, str):
            result = cls(indexstr=arg)
        elif isinstance(arg, cls):
            result = arg
        else:
            raise TypeError(f'Expected tuple, str, or {cls.__name__}, but got arg of type {type(arg).__name__}')
        if (array is not None) and (result.end_ci is None):
            return cls.from_start_and_array(result.start_ci, array)
        else:
            return result

    @classmethod
    def from_start_and_array(cls, topleft, array):
        '''returns CellRangeIndex starting at topleft and containing exactly enough space to fit 2d array.'''
        shape = list_shape(array) if isinstance(array, list) else array.shape
        assert len(shape) == 2, f"expected 2d array, got shape={shape}."
        start_ci = cls.ci_cls.implied_from(topleft)
        end_ci = start_ci + shape - (1,1)
        return cls(start_ci=start_ci, end_ci=end_ci)

    # # # BASIC PROPERTIES # # #
    def _set_start_ci(self, value):
        if not (value is None or isinstance(value, self.ci_cls)):
            raise TypeError(f'Expected type {self.ci_cls.__name__} for start_ci but got type {type(value).__name__}.')
        self._start_ci = value

    def _set_end_ci(self, value):
        if not (value is None or isinstance(value, self.ci_cls)):
            raise TypeError(f'Expected type {self.ci_cls.__name__} for end_ci but got type {type(value).__name__}.')
        self._end_ci = value

    def _set_index_ci(self, new_start_and_end_ci):
        self.start_ci, self.end_ci = new_start_and_end_ci

    def __iter__(self):
        yield self.start_ci
        yield self.end_ci

    start_ci = property(lambda self: getattr(self, '_start_ci', None), lambda self, val: self._set_start_ci(val),
                    doc='''CellIndex for top left cell. None if not known.''')
    end_ci = property(lambda self: getattr(self, '_end_ci', None), lambda self, val: self._set_end_ci(val),
                    doc='''CellIndex for bottom right cell. None if not known.''')
    index_ci = property(lambda self: (self.start_ci, self.end_ci), lambda self, val: self._set_index_ci(val),
                    doc='''(self.start_ci, self.end_ci). either or both can be None if not known.''')
    
    # # # MORE ADVANCED PROPERTIES # # #
    def _set_index(self, new_start_and_end):
        self.start, self.end = new_start_and_end

    start = property(lambda self: None if self.start_ci is None else self.start_ci.index,
                     lambda self, val: setattr(self, 'start_ci', self.ci_cls(index=val)),
                     doc='''0-indexed (row, col) of top left cell.''')
    end   = property(lambda self: None if self.end_ci is None else self.end_ci.index,
                     lambda self, val: setattr(self, 'end_ci', self.ci_cls(index=val)),
                     doc='''0-indexed (row, col) of bottom right cell.''')
    index = property(lambda self: (self.start, self.end),
                     lambda self, val: setattr(self, 'index_ci', self._set_index(val)),
                     doc='''0-indexed ((rowS, colS), (rowE, colE)) for S <--> top left; E <--> bottom right.''')

    def _set_indexstr(self, indexstr):
        splitted = indexstr.split(':')
        if len(splitted) == 0: splitted = ('', '')
        elif len(splitted) == 1: splitted = (splitted[0], '')
        NWstr, SEstr = splitted
        self.start_ci = self.ci_cls(indexstr=NWstr) if len(NWstr)>0 else None
        self.end_ci = self.ci_cls(indexstr=SEstr) if len(SEstr)>0 else None

    startstr = property(lambda self: self.start_ci.indexstr,
                      lambda self, val: setattr(self, 'startstr', self.ci_cls(indexstr=val)),
                      doc='''top left cell index as string. E.g. 'A1'.''')
    endstr = property(lambda self: self.end_ci.indexstr,
                      lambda self, val: setattr(self, 'endstr', self.ci_cls(indexstr=val)),
                      doc='''bottom right cell index as string. E.g. 'C7'.''')
    indexstr = property(lambda self: f'{self.startstr}:{self.endstr}',
                        lambda self, val: self._set_indexstr(val),
                        doc='''index string, E.g. 'A1:C7'.''')

    def _set_index1(self, new_start1_and_end1):
        self.start1, self.end1 = new_start1_and_end1

    start1 = property(lambda self: None if self.start_ci is None else self.start_ci.index1,
                      lambda self, val: setattr(self, 'start_ci', self.ci_cls(index1=val)),
                      doc='''1-indexed (row, col) of top left cell.''')
    end1   = property(lambda self: None if self.end_ci is None else self.end_ci.index1,
                      lambda self, val: setattr(self, 'end_ci', self.ci_cls(index1=val)),
                      doc='''1-indexed (row, col) of bottom right cell.''')
    index1 = property(lambda self: (self.start1, self.end1),
                      lambda self, val: setattr(self, 'index_xi', self._set_index1(val)),
                      doc='''1-indexed ((rowS, colS), (rowE, colE)) for S <--> top left; E <--> bottom right.''')

    def _get_slicer(self):
        NW, SE = self.index
        (N, W) = (None, None) if NW is None else NW
        (S, E) = (None, None) if SE is None else (SE[0]+1, SE[1]+1)
        return (slice(N, S), slice(W, E))

    slicer = property(lambda self: self._get_slicer(),
                      doc='''tuple of slices. If used, would provide values at range indicated by self.''')

    # # # DISPLAY # # #
    def __repr__(self):
        return f'{type(self).__name__}(start_ci={repr(self.start_ci)}, end_ci={repr(self.end_ci)})'

    def __str__(self):
        return repr(self.indexstr)


def rangestr_to_corners(s):
    '''returns ((start_row, start_col), (end_row, end_col)). (0-indexed)
    E.g. 'A4:B7' --> ((0,3),(1,6)).
    '''
    NWstr, SEstr = s.split(':')
    NW = cellstr_to_index(NWstr)
    SE = cellstr_to_index(SEstr)
    return (NW, SE)

def corners_to_rangestr(start, end):
    '''returns range string, given ((start_row, start_col), (end_row, end_col)). (0-indexed)
    E.g. ((0,3),(1,6)) --> 'A4:B7'.
    '''
    s_start = index_to_cellstr(start)
    s_end = index_to_cellstr(end)
    return f'{s_start}:{s_end}'

def rangestr_to_index(s):
    '''returns slices for cells given string; (row slice, column slice). (0-indexed)
    E.g. 'A4:B7' --> (slice(0, 2), slice(3, 7))
    '''
    (N, W), (S, E) = rangestr_to_corners(s)
    return (slice(N, S+1), slice(W, E+1))

def index_to_rangestr(i):
    '''returns range string given index'''
    raise NotImplementedError('[TODO]')


''' --------------------- Misc. Classes --------------------- '''

class Indexer():
    '''indexes input obj when __getitem__ is called.
    if finalize is not None, __getitem__ returns finalize(obj[i], i) instead of just obj[i].
    '''
    def __init__(self, obj, finalize=None):
        self.obj = obj
        self.finalize = finalize

    def __getitem__(self, i):
        result = self.obj[i]
        return result if self.finalize is None else self.finalize(result, i)

class Sentinel():
    '''alternative placeholder for function inputs'''
    def __repr__(self):
        return 'None2'

None2 = Sentinel()