"""
File Purpose: check equality between two objects.
"""

import warnings

from .imports import ImportFailed

try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')


''' --------------------- equals --------------------- '''

if isinstance(np, ImportFailed):
    def equals(x, y):
        '''check if x == y.'''
        return x == y
else:  # numpy imported successfully.
    def equals(x, y):
        '''check if x == y. Returns a boolean value, even if x or y is a numpy array.
        If either is a numpy array, return np.all(x == y) instead of x == y.
        '''
        result = (x == y)
        try:
            return bool(result)
        except ValueError:   # "The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()"
            return np.all(result)


''' --------------------- equals for lists, dicts, sets --------------------- '''
# return whether lists, dicts, sets are equal, using equals to check equality instead of ==

def list_equals(x, y, equals=equals):
    '''return whether lists x and y are equal.'''
    if len(x) != len(y):
        return False
    return all(equals(x[i], y[i]) for i in range(len(x)))

def dict_equals(x, y, equals=equals):
    '''return whether dicts x and y are equal.'''
    if x.keys() != y.keys():
        return False
    return all(equals(x[key], y[key]) for key in x.keys())

def equal_sets(x, y, equals=equals):
    '''returns whether x and y are equal in the sense of sets.
    equal when:
        for i in x, i in y, AND for j in y, j in x.

    result will be equivalent to Set(x) == Set(y), where Set is tools.sets.Set,
    however equal_sets(x, y) is more efficient in terms of speed.

    (not named set_equals to avoid ambiguity... this function doesn't *set* anything.)
    '''
    # loop through terms, pop same terms until all are popped or one is missing.
    y = [t for t in y]
    popped = []
    for term in x:
        j = 0
        found_match = False
        while j < len(y):
            if equals(term, y[j]):
                yjpop = y.pop(j)    # we found a match for y[j]; it is term.
                if not found_match:        # if this is the first match to term, record it,
                    popped.append(yjpop)   # so that later we can compare terms against popped.
                found_match = True
            else:
                j += 1
        if (not found_match) and (not any(equals(term, z) for z in popped)):
            return False
    if len(y) > 0:   # there is at least 1 element in y which was not in x.
        return False
    return True