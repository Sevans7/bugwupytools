"""
File Purpose: SlackChannel (for managing data from a slack channel)
"""

import json
import os
import re

from .channel_day import SlackChannelDay
from .messages import SlackMessages
from . import defaults

class SlackChannel(dict):
    '''Dict of data from all days in a slack channel.
    keys are days (as strings in YYYY-MM-DD format).
    values are SlackChannelDay objects (subclass of list).

    name: None or str; name of this channel.
    dirpath: None or str; abspath to the directory where this data came from.

    other args & kwargs go to dict.__init__.
    '''
    def __init__(self, *args_super, name=None, dirpath=None, **kw_super):
        self.name = name
        self.dirpath = None if dirpath is None else os.path.abspath(dirpath)
        super().__init__(*args_super, **kw_super)
        if defaults.POPULATE_MESSAGE_META:
            self.populate_message_meta()

    def populate_message_meta(self):
        '''add channel & day to each message in this channel'''
        for day in self.values():
            for message in day:
                message['channel'] = self.name

    @classmethod
    def from_files(cls, files, *, name=None):
        '''initialize from list of filepaths. Each filepath should be a day.json file.
        name: None or str
            name of this channel
        '''
        channel = cls(name=name)
        for filepath in files:
            day = SlackChannelDay.from_file(filepath)
            channel[day.day] = day
        if defaults.POPULATE_MESSAGE_META:
            channel.populate_message_meta()
        return channel

    @classmethod
    def from_dir(cls, dirpath):
        '''initialize from directory of day.json files.
        dirpath's base name should be the channel name.
        E.g. "announcements" dir with files "2024-07-01.json", "2024-07-02.json", etc.
            Announcments might contain other files; those will be ignored.
        '''
        name = os.path.basename(dirpath)
        files = [os.path.join(dirpath, f) for f in os.listdir(dirpath)]
        files = [f for f in files if re.match(r'\d{4}-\d{2}-\d{2}\.json', os.path.basename(f))]
        return cls.from_files(files, name=name)

    def _new(self, *args, **kw):
        '''return new instance of this class, like self'''
        result = type(self)(*args, **kw)
        if result.name is None:
            result.name = self.name
        return result

    def earliest(self):
        '''return the earliest day in this channel'''
        return min(self.keys())

    def latest(self):
        '''return the latest day in this channel'''
        return max(self.keys())
    
    def __repr__(self):
        contents = []
        if self.name is not None:
            contents.append(f'name={self.name!r}')
        if len(self)>0:
            contents.append(f'from {self.earliest()!r} to {self.latest()!r}')
        contents.append(f'n_days={self.n_channeldays}, n_messages={self.n_messages}')
        return f'{type(self).__name__}({", ".join(contents)})'

    # # # SIZE # # #
    @property
    def n_channeldays(self):
        '''return the number of days in this channel'''
        return len(self)

    @property
    def n_days(self):
        '''return the number of days in this channel'''
        return len(self)

    @property
    def n_messages(self):
        '''return the number of messages in this channel'''
        return sum(len(day) for day in self.values())

    # # # SUBSET # # #
    def on(self, *days):
        '''return Channel like self but keeping only the ChannelDays from these days.'''
        channel = self._new()
        for day in days:
            if day in self:
                channel[day] = self[day]
        return channel

    def between(self, start_day, stop_day, *, inclusive=True):
        '''return Channel like self but keeping only the ChannelDays in this range'''
        channel = self._new()
        for day in self.days:
            if (start_day < day < stop_day) or (inclusive and (day == start_day or day == stop_day)):
                channel[day] = self[day]
        return channel

    # # # ALIASES # # #
    @property
    def days(self):
        '''return set of all days in this channel.'''
        return sorted(self.keys())

    @property
    def messages(self):
        '''return all messages in this channel.'''
        return SlackMessages(message for channelday in self.values() for message in channelday)
