"""
File Purpose: messages data
"""

from ..tools import ListOfDicts

class SlackMessages(ListOfDicts):
    '''any list of messages'''
    @property
    def n_messages(self):
        '''return the number of messages in this data'''
        return len(self)

    def __repr__(self):
        return f'{type(self).__name__}(n_messages={self.n_messages})'

    def compute_n_words(self):
        '''add n_words to each message in this data.
        return self['n_words'].
        '''
        for message in self:
            message['n_words'] = len(message['text'].split())
        return self['n_words']

    def populate_real_names(self, users=None):
        '''add real_name to each message in this data.
        if users is provided, use users and message['user'] to get real_name.
        otherwise, use real_name from message['user_profile']['real_name'] if possible.'''
        if users is None:
            for message in self:
                user_profile = message.get('user_profile', {})
                message['real_name'] = user_profile.get('real_name', None)
        else:
            lookup = {user['id']: user for user in users}
            for message in self:
                user_id = message.get('user', None)  # None <--> e.g., bot instead of person
                user = lookup.get(user_id, {})
                message['real_name'] = user.get('real_name', None)
        return self
