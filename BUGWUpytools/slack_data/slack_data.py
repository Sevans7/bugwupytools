"""
File Purpose: SlackData (all the slack data)
"""

import builtins
import os

from .channel import SlackChannel
from .channel_day import SlackChannelDay
from .messages import SlackMessages
from .users import SlackUsers, SlackUsersWithData
from . import defaults


class SlackData(dict):
    '''Dict of all slack data.
    keys are channel names.
    values are SlackChannel objects.
    
    users: None SlackUsers; all the slack users (if known).
    name: None or str; name of this data.
    dirpath: None or str; abspath to the directory where this data came from.
    
    other args & kwargs go to dict.__init__.
    '''
    def __init__(self, *args_super, users=None, name=None, dirpath=None, **kw_super):
        self.users = users
        self.name = name
        self.dirpath = None if dirpath is None else os.path.abspath(dirpath)
        super().__init__(*args_super, **kw_super)
        if defaults.POPULATE_MESSAGE_META:
            self.populate_message_meta()

    def populate_message_meta(self):
        '''add users' real_name to messages in self'''
        self.messages.populate_real_names(self.users)
    
    @classmethod
    def from_dirs(cls, dirs, *, name=None, users=None):
        '''initialize from list of channel dirpaths.
        dirs: list of str
            each str is an abspath to a channel dir.
        name: None or str
            name of this data
        '''
        data = cls(name=name, users=users)
        for channel_dir in dirs:
            channel = SlackChannel.from_dir(channel_dir)
            data[channel.name] = channel
        if defaults.POPULATE_MESSAGE_META:
            data.populate_message_meta()
        return data

    @classmethod
    def from_dir(cls, dirpath, *, keep_deleted_users=False,):
        '''initialize from directory of channel dirs & users.json file.
        dirpath's base name should be the data name.
        E.g. "slack_data" dir with dirs "announcements", "general", etc.
            Each channel dir should contain day.json files.

        keep_deleted_users: bool; whether to keep deleted users.
        '''
        name = os.path.basename(dirpath)
        dirs = [os.path.join(dirpath, d) for d in os.listdir(dirpath)]
        dirs = [d for d in dirs if os.path.isdir(d)]
        users = SlackUsers.from_dir(dirpath, keep_deleted=keep_deleted_users)
        return cls.from_dirs(dirs, name=name, users=users)

    def _new(self, *args, **kw):
        '''return new instance of this class, like self'''
        result = type(self)(*args, **kw)
        if result.users is None:
            result.users = self.users
        return result
    
    def __repr__(self):
        contents = []
        if self.name is not None:
            contents.append(f'name={self.name!r}')
        contents.append(f'n_channels={self.n_channels}')
        contents.append(f'n_days={self.n_days}')
        contents.append(f'n_messages={self.n_messages}')
        contents.append(f'users={self.users}')
        return f'{type(self).__name__}({", ".join(contents)})'

    # # # SIZE # # #
    @property
    def n_channels(self):
        '''return the number of channels in this data'''
        return len(self)

    @property
    def n_channeldays(self):
        '''return the number of days in this data'''
        return sum(channel.n_channeldays for channel in self.values())

    @property
    def n_messages(self):
        '''return the number of messages in this data'''
        return sum(channel.n_messages for channel in self.values())

    @property
    def n_days(self):
        '''return the number of days in this data'''
        return len(self.days)

    @property
    def n_users(self):
        '''return the number of users in this data'''
        return 0 if self.users is None else len(self.users)

    # # # SUBSET # # #
    def on(self, *days):
        '''return SlackData like self but keeping only data from these days.
        channels with no data from any of these days will be dropped.
        '''
        data = self._new()
        for name, channel in self.items():
            if any(day in channel for day in days):
                data[name] = channel.on(*days)
        return data

    def between(self, start_day, end_day, *, inclusive=True):
        '''return SlackData like self but keeping only data from start_day to end_day (inclusive).
        channels with no data from any of these days will be dropped.
        '''
        data = self._new()
        for name, channel in self.items():
            channel_between = channel.between(start_day, end_day, inclusive=inclusive)
            if len(channel_between)>0:
                data[name] = channel_between
        return data

    # # # ALIASES # # #
    @property
    def channels(self):
        '''return list of all channel names in this data.'''
        return sorted(self.keys())

    @property
    def days(self):
        '''return list of all days in this data.'''
        return sorted(set(day for channel in self.values() for day in channel.days))

    @property
    def messages(self):
        '''return list of all messages in this data.'''
        return sum((channel.messages for channel in self.values()), start=SlackMessages())

    # # # COUNTING STUFF # # #
    def user_data_result(self, data_key, id_to_value):
        '''create & return a SlackUsersWithData object from self.users, id_to_value, and data_key.
        id_to_value: dict
            keys are user ids; values are values to add to each user, at this key
        '''
        return SlackUsersWithData.from_existing(self.users, id_to_value, data_key=data_key)

    # NUMBER OF MESSAGES PER USER #
    def user_id_message_counts(self):
        '''return dict of user id -> message count.
        counts are across all channels and all days.
        '''
        counts = dict()
        for message in self.messages:
            try:
                user_id = message['user']
            except KeyError:
                pass  # no user for this message; skip it.
            if user_id not in counts:
                counts[user_id] = 1
            else:
                counts[user_id] += 1
        return counts

    def user_message_counts(self, min=1):
        '''return SlackUsersWithData, with 'message_counts' key telling how many messages each user sent.
        min: int or None, default 1
            only keep users with at least this many messages.
            None --> keep all users.
        '''
        counts = self.user_id_message_counts()
        result = self.user_data_result('message_counts', counts)
        if min is not None:
            result = result.where('message_counts', lambda x: x>=min, default=min-1)
        return result

    def user_message_scoreboard(self, min=1):
        '''return real_name, message_counts for users, sorted by message_counts.
        min: int or None, default 1
            only keep users with at least this many messages.
            None --> keep all users.
        '''
        counts = self.user_message_counts(min=min)
        return counts.sortby('message_counts', reverse=True).get(['real_name', 'message_counts'])

    # NUMBER OF WORDS PER USER #
    def user_id_word_counts(self):
        '''return dict of user id -> word count.
        counts are across all channels and all days.
        Also computes n_words for each message in self.messages.
        '''
        counts = dict()
        messages = self.messages
        messages.compute_n_words()
        for message in messages:
            try:
                user_id = message['user']
            except KeyError:
                pass
            if user_id not in counts:
                counts[user_id] = message['n_words']
            else:
                counts[user_id] += message['n_words']
        return counts

    def user_word_counts(self, min=1):
        '''return SlackUsersWithData, with 'word_counts' key telling how many words each user sent.
        min: int or None, default 1
            only keep users with at least this many words.
            None --> keep all users.
        '''
        counts = self.user_id_word_counts()
        result = self.user_data_result('word_counts', counts)
        if min is not None:
            result = result.where('word_counts', lambda x: x>=min, default=min-1)
        return result

    def user_word_scoreboard(self, min=1):
        '''return real_name, word_counts for users, sorted by word_counts.
        min: int or None, default 1
            only keep users with at least this many words.
            None --> keep all users.
        '''
        counts = self.user_word_counts(min=min)
        return counts.sortby('word_counts', reverse=True).get(['real_name', 'word_counts'])

    # NUMBER OF ESSAYS PER USER #
    def user_id_essay_counts(self, essay_n_words=100):
        '''return dict of user id -> number of essays.
        "essay" if message has at least essay_n_words words.
        counts are across all channels and all days.
        Also computes n_words for each message in self.messages.
        '''
        counts = dict()
        messages = self.messages
        messages.compute_n_words()
        for message in messages:
            try:
                user_id = message['user']
            except KeyError:
                pass
            if message['n_words'] >= essay_n_words:
                if user_id not in counts:
                    counts[user_id] = 1
                else:
                    counts[user_id] += 1
        return counts

    def user_essay_counts(self, essay_n_words=100, *, min=1):
        '''return SlackUsersWithData, with 'essay_counts' key telling how many essays each user sent.
        min: int or None, default 1
            only keep users with at least this many essays.
            None --> keep all users.
        '''
        counts = self.user_id_essay_counts(essay_n_words=essay_n_words)
        result = self.user_data_result('essay_counts', counts)
        if min is not None:
            result = result.where('essay_counts', lambda x: x>=min, default=min-1)
        return result

    def user_essay_scoreboard(self, essay_n_words=100, *, min=1):
        '''return real_name, essay_counts for users, sorted by essay_counts.
        min: int or None, default 1
            only keep users with at least this many essays.
            None --> keep all users.
        '''
        counts = self.user_essay_counts(essay_n_words=essay_n_words, min=min)
        return counts.sortby('essay_counts', reverse=True).get(['real_name', 'essay_counts'])

    # AVERAGE NUMBER OF WORDS PER USER MESSAGE #
    def user_id_avg_word_counts(self, *, round=None):
        '''return dict of user id -> average number of words per message.
        counts are across all channels and all days.
        Also computes n_words for each message in self.messages.

        round: None or int
            if int, round to this many decimal places.
        '''
        result = dict()
        messages = self.messages
        messages.compute_n_words()
        for message in messages:
            try:
                user_id = message['user']
            except KeyError:
                pass
            n_words = message['n_words']
            if user_id not in result:
                result[user_id] = n_words
            else:
                result[user_id] += n_words
        message_counts = self.user_id_message_counts()
        for user_id in result:
            result[user_id] /= message_counts.get(user_id, 1)
            if round is not None:
                result[user_id] = builtins.round(result[user_id], round)
        return result

    def user_avg_word_counts(self, min_messages=1, *, round=None):
        '''return SlackUsersWithData, with 'avg_word_counts' key telling average words per message.
        min_messages: int or None, default 1
            only keep users with at least this many messages.
            None --> keep all users.
        round: None or int
            if int, round to this many decimal places.
        '''
        counts = self.user_id_avg_word_counts(round=round)
        result = self.user_data_result('avg_word_counts', counts)
        if min_messages is not None:
            message_counts = self.user_id_message_counts()
            result = result.where('id', lambda id_: message_counts.get(id_, 0)>=min_messages)
        return result

    def user_avg_word_scoreboard(self, min_messages=1, *, round=None):
        '''return real_name, avg_word_counts for users, sorted by avg_word_counts.
        min_messages: int or None, default 1
            only keep users with at least this many messages.
            None --> keep all users.
        round: None or int
            if int, round to this many decimal places.
        '''
        counts = self.user_avg_word_counts(min_messages=min_messages, round=round)
        return counts.sortby('avg_word_counts', reverse=True).get(['real_name', 'avg_word_counts'])
