"""
File Purpose: SlackChannelDay (for managing data from one day in a slack channel)
"""

import json
import os

from .messages import SlackMessages
from . import defaults


class SlackChannelDay(SlackMessages):
    '''List of data from one day in a slack channel.
    
    filepath: None or abspath to file where this data came from.
    '''
    def __init__(self, day, *data, filepath=None):
        self.day = day
        self.filepath = None if filepath is None else os.path.abspath(filepath)
        super().__init__(*data)
        if defaults.POPULATE_MESSAGE_META:
            self.populate_message_meta()

    def populate_message_meta(self):
        '''add day to each message in this day'''
        for message in self:
            message['day'] = self.day

    @classmethod
    def from_file(cls, filepath):
        '''initialize from filepath, whose basename should be day.json. E.g. 2024-07-01.json.'''
        day = os.path.basename(filepath).split('.')[0]
        with open(filepath, 'r') as f:
            data = json.load(f)
        return cls(day, data, filepath=filepath)

    def __repr__(self):
        return f'{type(self).__name__}(day={self.day!r}, n_messages={self.n_messages})'
