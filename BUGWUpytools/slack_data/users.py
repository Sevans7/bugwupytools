"""
File Purpose: the slack users data
"""

import copy
import os
import json

from ..tools import ListOfDicts


class SlackUsers(ListOfDicts):
    '''list of all slack users, probably from the json file users.json.
    
    keep_deleted: bool; whether to keep deleted users, when initializing.
    '''
    def __init__(self, *args_super, keep_deleted=False, **kw_super):
        super().__init__(*args_super, **kw_super)
        if not keep_deleted:
            self.remove_deleted()

    @classmethod
    def from_file(cls, filepath, **kw_init):
        '''initialize from filepath, whose basename should be users.json'''
        with open(filepath, 'r') as f:
            data = json.load(f)
        return cls(data, **kw_init)

    @classmethod
    def from_dir(cls, dirpath, **kw_init):
        '''initialize from directory containing users.json'''
        filepath = os.path.join(dirpath, 'users.json')
        return cls.from_file(filepath, **kw_init)

    def __repr__(self):
        return f'{type(self).__name__}(n_users={len(self)})'

    def _new(self, *args, **kw):
        '''return new instance of this class, like self'''
        return type(self)(*args, **kw)

    def remove_deleted(self):
        '''removes all users with user['deleted'] == True.'''
        self[:] = [user for user in self if not user.get('deleted', False)]

    # # # SIZE # # #
    @property
    def n_users(self):
        '''return the number of users'''
        return len(self)


class SlackUsersWithData(SlackUsers):
    '''SlackUsers with additional data possibly added directly to the user dicts.
    Makes deepcopy of user dicts when initializing.
    '''
    def __init__(self, *args_super, **kw_super):
        super().__init__(*args_super, keep_deleted=True, **kw_super)  # don't remove deleted at this step...

    @classmethod
    def from_existing(cls, slack_users, id_to_data, *, data_key=None):
        '''initialize from existing slack_users and data.
        Makes deepcopy of user dicts.

        slack_users: SlackUsers
            existing SlackUsers object
        id_to_data: dict
            keys are user ids; values are data to add to each user.
        data_key: None or str
            None --> values of id_to_data must be dicts; user.update(id_to_data[user['id']])
            else --> user[data_key] = id_to_data[user['id']]

        '''
        data = [copy.deepcopy(user) for user in slack_users]
        for user in data:
            if user['id'] in id_to_data:
                if data_key is not None:
                    user[data_key] = id_to_data[user['id']]
                else:
                    user.update(id_to_data[user['id']])
        return cls(data)
